/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100132
Source Host           : localhost:3306
Source Database       : alumination_ph

Target Server Type    : MYSQL
Target Server Version : 100132
File Encoding         : 65001

Date: 2019-02-12 21:52:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alph_accounts
-- ----------------------------
DROP TABLE IF EXISTS `alph_accounts`;
CREATE TABLE `alph_accounts` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `salt` text NOT NULL,
  `usertype` tinyint(1) NOT NULL COMMENT '1 - admin, 2 - seller, 3 - customer',
  `business_name` varchar(255) NOT NULL,
  `business_address` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of alph_accounts
-- ----------------------------
INSERT INTO `alph_accounts` VALUES ('1', 'Admin', 'alph.admin@gmail.com', '00000000000', 'admin address', '21e044c1aee0ccd214e5cac072067e37f76aad30b81413635e30630d14c2911d', '45135f57718206a0', '1', '', '');

-- ----------------------------
-- Table structure for alph_products
-- ----------------------------
DROP TABLE IF EXISTS `alph_products`;
CREATE TABLE `alph_products` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 : doors, 2 : window, 3 : gate, 4 : railings',
  `description` text NOT NULL,
  `images` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - not available, 1 - available',
  `creator` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `processing` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - pending, 1 - approved',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of alph_products
-- ----------------------------

-- ----------------------------
-- Table structure for alph_requests
-- ----------------------------
DROP TABLE IF EXISTS `alph_requests`;
CREATE TABLE `alph_requests` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 - canceled, 1 - pending, 2 - done',
  `image` text NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of alph_requests
-- ----------------------------
