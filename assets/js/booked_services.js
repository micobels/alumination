$(document).on('click','.upload_resibo',function(){
	id = $(this).attr('data-id');
	$('#upload_images').find('.upload_proceed').attr('data-id',id);
	$('#upload_images').modal();
})

$(document).on('submit','#upload_reciept_form',function(e){
	e.preventDefault();
	id = $('.upload_proceed').attr('data-id');
	form_data = new FormData(e.target);
	form_data.append('id',id);
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Booked_services/upload_receipt",
		data 	: form_data,
		cache	: false,
		dataType : 'json',
		processData: false,
		contentType: false,
		success : function(r){
			$('#success_upload').modal();
			$('#upload_images').modal('hide');
			page = eval($('.cur_page').text());
			type = $('.pagination').attr('data-type');
			load_table(page,type);
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$('#upload_reciept_image').change(function(){
	readURL(this);
})

function readURL(input) {
	if(input.files){
		$('.preview_area').empty();
		for (var i = input.files.length - 1; i >= 0; i--) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('.preview_area').append('<img src="'+e.target.result+'">');
			}
			reader.readAsDataURL(input.files[i]);
		}
	}
}

$(document).on('click','.customer_support',function(){
	$('#message_modal').modal();
})

$(document).on('click','.view_image',function(){
	id = $(this).attr('data-id');
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Admin/Services/retrieve_image",
		data 	: {'id' : id},
		cache	: false,
		dataType : 'json',
		success : function(r){
			console.log(r);
			$('#preview_image').find('img').attr('src',baseUrl+r.list.image);
			$('#preview_image').modal();
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$(document).on('click','.approve_trans',function(){
	id = $(this).attr('data-id');
	$('#approve_prod_modal').find('.proceed_approve').attr('data-id',id);
	$('#approve_prod_modal').modal();
})

$('.proceed_approve').click(function(){
	id = $(this).attr('data-id');
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Admin/Services/approve_prod",
		data 	: {'id' : id},
		cache	: false,
		dataType : 'json',
		success : function(r){
			$('#approve_prod_modal').modal('hide');
			$('.success_notify').find('p').text('Transaction Approved.');
			$('.success_notify').show();
			setTimeout(function(){
				$('.success_notify').fadeOut();
			},3000);

			page = eval($('.cur_page').text());
			type = $('.pagination').attr('data-type');
			load_table(page,type);
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$(document).on('click','.finish_service',function(){
	id = $(this).attr('data-id');
	$('#finish_trans_modal').find('.finish_trans').attr('data-id',id);
	$('#finish_trans_modal').modal();
})

$('.finish_trans').click(function(){
	id = $(this).attr('data-id');
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Admin/Services/finish_trans",
		data 	: {'id' : id},
		cache	: false,
		dataType : 'json',
		success : function(r){
			$('#finish_trans_modal').modal('hide');
			$('.success_notify').find('p').text('Transaction Finished.');
			$('.success_notify').show();
			setTimeout(function(){
				$('.success_notify').fadeOut();
			},3000);

			page = eval($('.cur_page').text());
			type = $('.pagination').attr('data-type');
			load_table(page,type);
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$(document).on('click','.preview_feedback',function(){
	id = $(this).attr('data-id');
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Admin/Services/retrieve_feedback",
		data 	: {'id' : id},
		cache	: false,
		dataType : 'json',
		success : function(r){
			console.log(r);
			$('#preview_feedback').find('.modal-body').html(r.list.feedback == null ? 'No Feedbacks Placed.' : r.list.feedback);
			$('#preview_feedback').modal();
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$(document).on('click','.cancel_service',function(){
	id = $(this).attr('data-id');
	$('#cancel_confirmation').find('.proceed_cancelling').attr('data-id',id);
	$('#cancel_confirmation').modal();
})

$('.proceed_cancelling').click(function(){
	id = $(this).attr('data-id');
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Booked_services/cancel_service",
		data 	: {'id' : id},
		cache	: false,
		dataType : 'json',
		success : function(r){
			$('#cancel_confirmation').modal('hide');
			$('.success_notify').find('p').text('Service Successfully Cancelled');
			$('.success_notify').show();
			setTimeout(function(){
				$('.success_notify').fadeOut();
			},3000);

			page = eval($('.cur_page').text());
			type = $('.pagination').attr('data-type');
			load_table(page,type);
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$(document).on('click','.uncancel',function(){
	id = $(this).attr('data-id');
	$('#uncancel_confirmation').find('.proceed_uncancelling').attr('data-id',id);
	$('#uncancel_confirmation').modal();
})

$('.proceed_uncancelling').click(function(){
	id = $(this).attr('data-id');
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Booked_services/uncancel_service",
		data 	: {'id' : id},
		cache	: false,
		dataType : 'json',
		success : function(r){
			$('#uncancel_confirmation').modal('hide');
			$('.success_notify').find('p').text('Service Successfully Recalled');
			$('.success_notify').show();
			setTimeout(function(){
				$('.success_notify').fadeOut();
			},3000);

			page = eval($('.cur_page').text());
			type = $('.pagination').attr('data-type');
			load_table(page,type);
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$(document).on('click','.paginate',function(){
	cur_page = eval($('.pagination').find('.cur_page').text());
	max_page = eval($('.pagination').find('.max_page').text());
	type = $('.pagination').attr('data-type');
	if($(this).hasClass('next_page')){
		if(cur_page < max_page){
			cur_page = cur_page + 1;
			load_table(cur_page,type);
		}
	}
	if($(this).hasClass('prev_page')){
		if(cur_page > 1){
			cur_page = cur_page - 1;
			load_table(cur_page,type);
		}
	}
})

function load_table(page,type){
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Admin/Services/load_table",
		data 	: {
			'page' : page,
			'type' : type
		},
		cache	: false,
		dataType : 'html',
		success : function(r){
			$('.cur_page').text(page);
			$('table tbody').html(r);
		},
		error 	: function(e){
			console.log('error');
		}
	})
}

$(document).on('click','.open_feedback',function(){
	id = $(this).attr('data-id');

	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Booked_services/retrieve_feedback",
		data 	: {'id' : id},
		cache	: false,
		dataType : 'json',
		success : function(r){
			// console.log(r.list.feedback);
			if(r.list[0].feedback != ''){
				$('#add_feedback').find('textarea').val(r.list[0].feedback);
			}
			$('#add_feedback').find('.save_feedback').attr('data-id',id);
			$('#add_feedback').modal();
		},
		error 	: function(e){
			console.log('error');
		}
	})
})

$('.save_feedback').click(function(){
	id = $(this).attr('data-id');
	feedback = $('#add_feedback').find('textarea').val();
	$.ajax({
		type  	: 'POST',
		url 	: baseUrl+"Booked_services/submit_feedback",
		data 	: {'id' : id,'feedback' : feedback},
		cache	: false,
		dataType : 'json',
		success : function(r){
			$('#add_feedback').modal('hide');
			$('.success_notify').find('p').text('Feedback Successfully Submited');
			$('.success_notify').show();
			setTimeout(function(){
				$('.success_notify').fadeOut();
			},3000);

			page = eval($('.cur_page').text());
			type = $('.pagination').attr('data-type');
			load_table(page,type);
		},
		error 	: function(e){
			console.log('error');
		}
	})
})