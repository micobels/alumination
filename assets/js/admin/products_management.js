$(function(){
	$(document).on('submit', '#create_product_form', function(e){
		e.preventDefault();
		var form = $('.create_product_form');
		var product = {
			'name' : form.find('#prod_name').val(),
			'type' : form.find('#prod_type').val(),
			'desc' : form.find('#prod_description').val(),
		}

		form_data = new FormData(e.target);
		for ( var key in product ) {
		    form_data.append(key, product[key]);
		}

		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Products/save_product",
			data 	: form_data,
			cache	: false,
			dataType : 'json',
			processData: false,
			contentType: false,
			success : function(r){
				$('.success_notify').find('p').text('Successfully Created a New Product.');
				$('.success_notify').show();
				setTimeout(function(){
					$('.success_notify').fadeOut();
				},3000);
				reset_form();
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	function reset_form(){
		$('#prod_name,#prod_description,#upload_prod_images').val('');
		$('#prod_type').val(0);
		$('.img_preview_container').empty();
	}

	$(document).on('click', '.edit_prod', function(){
		prod_id = $(this).attr('data-id');
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Products/get_data_prod",
			data 	: {
				'id' : prod_id
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#edit_product_modal').find('#prod_name').val(r.list[0].name);
				$('#edit_product_modal').find('#prod_type').val(r.list[0].type);
				$('#edit_product_modal').find('#prod_description').val(r.list[0].description);
				$('#edit_product_modal').find('.save_edit').attr('data-id',r.list[0].pid);
				$('#edit_product_modal').modal();
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click', '.save_edit', function(){
		prod_id = $(this).attr('data-id');
		prod_name = $('#edit_product_modal').find('#prod_name').val();
		prod_type = $('#edit_product_modal').find('#prod_type').val();
		prod_desc = $('#edit_product_modal').find('#prod_description').val();

		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Products/save_edit_prod",
			data 	: {
				'id' : prod_id,
				'name' : prod_name,
				'type' : prod_type,
				'desc' : prod_desc
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#edit_product_modal').modal('hide');
				$('.success_notify').find('p').text('Successfully Saved Edits.');
				$('.success_notify').show();
				setTimeout(function(){
					$('.success_notify').fadeOut();
				},3000);
				cur_page = eval($('.pagination').find('.cur_page').text());
				status = $('.pagination').attr('data-status');
				load_table(cur_page,status);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click','.delete_product',function(){
		prod_id = $(this).attr('data-id');
		$('#confirm_delete_prod').find('.proceed_delete').attr('data-id',prod_id);
		$('#confirm_delete_prod').modal();
	})

	$(document).on('click','.proceed_delete',function(){
		prod_id = $(this).attr('data-id');
		console.log(prod_id);
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Products/delete_prod",
			data 	: {
				'id' : prod_id,
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#confirm_delete_prod').modal('hide');
				$('.success_notify').find('p').text('Successfully Deleted Product.');
				$('.success_notify').show();
				setTimeout(function(){
					$('.success_notify').fadeOut();
				},3000);
				cur_page = eval($('.pagination').find('.cur_page').text());
				status = $('.pagination').attr('data-status');
				load_table(cur_page,status);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click','.paginate',function(){
		cur_page = eval($('.pagination').find('.cur_page').text());
		max_page = eval($('.pagination').find('.max_page').text());
		status = $('.pagination').attr('data-status');
		if($(this).hasClass('next_page')){
			if(cur_page < max_page){
				cur_page = cur_page + 1;
				load_table(cur_page,status);
			}
		}
		if($(this).hasClass('prev_page')){
			if(cur_page > 1){
				cur_page = cur_page - 1;
				load_table(cur_page,status);
			}
		}
	})

	function load_table(page,status){
		console.log(page);
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Products/load_table",
			data 	: {
				'page' : page,
				'status' : status
			},
			cache	: false,
			dataType : 'html',
			success : function(r){
				$('table tbody').html(r);
				$('.pagination').find('.cur_page').text(cur_page);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	}

	$(document).on('click','.activate_prod',function(){
		prod_id = $(this).attr('data-id');
		$('#confirm_activation').find('.proceed_activation').attr('data-id',prod_id);
		$('#confirm_activation').modal();
	})

	$(document).on('click','.proceed_activation',function(){
		prod_id = $(this).attr('data-id');
		console.log(prod_id);
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Products/activate_prod",
			data 	: {
				'id' : prod_id,
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#confirm_activation').modal('hide');
				$('.success_notify').find('p').text('Successfully Activated Product.');
				$('.success_notify').show();
				setTimeout(function(){
					$('.success_notify').fadeOut();
				},3000);
				cur_page = eval($('.pagination').find('.cur_page').text());
				status = $('.pagination').attr('data-status');
				load_table(cur_page,status);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	function readURL(input) {
		if(input.files){
			$('.img_preview_container').empty();
			for (var i = input.files.length - 1; i >= 0; i--) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.img_preview_container').append('<li><img src="'+e.target.result+'"></li>');
				}
				reader.readAsDataURL(input.files[i]);
			}
		}
	}

	$('#upload_prod_images').on('change', function () {
		readURL(this);	
	});


})