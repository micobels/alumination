$(function(){
	$(document).on('click', '.proceed_registration', function(){
		var form = $('.admin_registration_form');

		// validate
		if(form.find('#user_pass').val() != form.find('#confirm_pass').val()){
			console.log("password not match");
			return false;
		}
		if(form.find('#user_type').val() == 0){
			console.log("choose user type");
			return false;
		}

		var registration_data = {
			'fullname' 			: form.find('#user_name').val(),
			'usertype' 			: form.find('#user_type').val(),
			'email' 			: form.find('#user_email').val(),
			'mobile' 			: form.find('#user_mobile').val(),
			'address' 			: form.find('#user_address').val(),
			'password' 			: form.find('#user_pass').val(),
			'password' 			: form.find('#user_pass').val(),
			'business_name' 	: form.find('#user_business_name').val(),
			'business_address' 	: form.find('#user_business_address').val(),
		}

		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Accounts/save_account",
			data 	: {
				'info' : registration_data
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				console.log('success');
			},
			error 	: function(e){
				console.log('error');
			}
		})
	});

	$(document).on('click', '.edit_account', function(){
		var user_id = $(this).attr('data-uid');
		var user_utype = $(this).attr('data-utype');
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Accounts/get_user_data?type="+user_utype,
			data 	: {
				'id' : user_id
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				console.log(r);
				var form = $('.edit_account_form');
				form.find('#edit_fname').val(r.list[0].fname);
				form.find('#edit_mname').val(r.list[0].mname);
				form.find('#edit_lname').val(r.list[0].lname);
				form.find('#edit_address_no').val(r.list[0].address_no);
				form.find('#edit_address_barangay').val(r.list[0].address_barangay);
				form.find('#edit_address_city').val(r.list[0].address_city);
				form.find('#edit_address_state').val(r.list[0].address_state);
				form.find('#edit_mobile').val(r.list[0].mobile);
				form.find('#edit_email').val(r.list[0].email);
				form.find('#edit_business_name').val(r.list[0].business_name);
				form.find('#edit_business_address').val(r.list[0].business_address);
				$('.save_edit_account').attr('data-uid', user_id);
				$('.save_edit_account').attr('data-utype', r.list[0].usertype);

				if(r.list[0].usertype == 2){
					$('.business_info_contain').show();
				}
				$('#edit_product_modal').modal();
			},
			error 	: function(e){
				console.log('error');
			}
		})
	});

	$(document).on('click', '.activate_account', function(){
		var user_id = $(this).attr('data-uid');
		var user_utype = $(this).attr('data-utype');
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Registration_payment/fetch_image",
			data 	: {
				'id' : user_id
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				console.log(r);
				$('#view_activate_details').find('img').attr('src',baseUrl+r.list[0].receipt_image);
				$('#view_activate_details').find('.validate_acc_payment').attr('data-uid',user_id);
				$('#view_activate_details').find('.validate_acc_payment').attr('data-utype',user_utype);
				$('#view_activate_details').modal();
			},
			error 	: function(e){
				console.log('error');
			}
		})
	});

	$(document).on('click','.validate_acc_payment',function(){
		var user_id = $(this).attr('data-uid');
		var user_utype = $(this).attr('data-utype');
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Registration_payment/validate_acc_payment",
			data 	: {
				'id' : user_id
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				console.log(r);
				$('#view_activate_details').modal('hide');
				reload_table($('.account_list_pagination').find('.current_page').text(), user_utype);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click', '.save_edit_account', function(){
		var form = $('.edit_account_form');
		var utype = $(this).attr('data-utype')
		edit_content = {
			'id' : $(this).attr('data-uid'),
			'utype' : utype,
			'fname' : form.find('#edit_fname').val(),
			'mname' : form.find('#edit_mname').val(),
			'lname' : form.find('#edit_lname').val(),
			'address_no' : form.find('#edit_address_no').val(),
			'address_barangay' : form.find('#edit_address_barangay').val(),
			'address_city' : form.find('#edit_address_city').val(),
			'address_state' : form.find('#edit_address_state').val(),
			'mobile' : form.find('#edit_mobile').val(),
			'email' : form.find('#edit_email').val(),
			'business_name' : form.find('#edit_business_name').val(),
			'business_address' : form.find('#edit_business_address').val(),
		}

		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Accounts/update_user_data",
			data 	: {
				'content' : edit_content
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#edit_product_modal').modal('hide');
				reload_table($('.account_list_pagination').find('.current_page').text(), utype);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	});

	$(document).on('click', '.delete_account', function(){
		var id = $(this).attr('data-uid');
		var utype = $(this).attr('data-utype');
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Accounts/delete_user_data",
			data 	: {
				'id' : id,
				'utype' : utype
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				reload_table($('.account_list_pagination').find('.current_page').text(), utype);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	});

	$(document).on('change', '#user_type', function(){
		if($(this).val() == 2){
			$('.business_info_container').show();
		}else{
			$('.business_info_container').hide();
		}
	});

	$(document).on('click', '.page', function(){
		var active_page =  $('.account_list_pagination').find('.current_page').text();
		var total_page =  $('.account_list_pagination').find('.total_page').text();

		if($(this).hasClass('prev_page')){
			console.log('asdasd');
			if(active_page <= 1){
				return false;
			}
			to_page = parseInt(active_page) - 1;
		}else if ($(this).hasClass('next_page')){
			if(active_page >= total_page){
				return false;
			}
			to_page = parseInt(active_page) + 1;
		}
		$('.account_list_pagination').find('.current_page').text(to_page);
		reload_table(to_page);
	})

	$(document).on('click', '.search_accounts', function(){
		var params = $('.search_input').val();
		var utype = $(this).attr('data-utype');

		if(params){
			reload_table(1, utype, params);
		}
	})

	function reload_table(page, type = '', params = ''){
		console.log(params);
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Admin/Accounts/load_table?type="+type,
			data 	: {
				'page' : page,
				'params' : params
			},
			cache	: false,
			dataType : 'html',
			success : function(r){
				$('.account_list_table').html(r);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	};
})