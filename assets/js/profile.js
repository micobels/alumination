$('.proceed_edits').click(function(){
	proceed = true;
	$('.validate').each(function(){
		if($(this).val() == ''){
			$('.error_notify').find('p').text('Please Fill up all the Missing Fields');
			$('.error_notify').show();
			setTimeout(function(){
				$('.error_notify').fadeOut();
			},3000);
			proceed = false;
			return false;
		}
	})

	if($('#user_pass').val() != $('#confirm_pass').val()){
		$('.error_notify').find('p').text('New Password Does not Match');
		$('.error_notify').show();
		setTimeout(function(){
			$('.error_notify').fadeOut();
		},3000);
		proceed = false;
	}

	if(proceed){

		new_data = {
			'fname' : $('#user_fname').val(),
			'mname' : $('#user_mname').val(),
			'lname' : $('#user_lname').val(),
			'email' : $('#user_email').val(),
			'mobile' : $('#user_mobile').val(),
			'address' : $('#user_address').val()
		}
		if($('.business_info_container')){
			new_data['bus_name'] = $('#user_business_name').val();
			new_data['bus_add'] = $('#user_business_address').val();
		}
		if($('#user_pass').val() != ''){
			new_data['password'] = $('#user_pass').val();
		}
		console.log(new_data);
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Profile/edit_profile",
			data 	: new_data,
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('.success_notify').find('p').text('Account Edit Successfully');
				$('.success_notify').show();
				setTimeout(function(){
					$('.success_notify').fadeOut();
					window.location.href= baseUrl+'Login';
				},3000);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	}
})