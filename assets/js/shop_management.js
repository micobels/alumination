$(function(){
	$(document).on('click', '.page', function(){
		var page = $('.active_page').text();
		var page_total = $('.page_total').text();
		var type = $(this).attr('data-utype');

		if($(this).hasClass('prev_page')){
			if(page <= 1){
				return false;
			}
			var to_page = parseInt(page) - 1;
		}else if($(this).hasClass('next_page')){
			if(page >= page_total){
				return false;
			}
			var to_page = parseInt(page) + 1;
		}
		$('.active_page').text(to_page)

		paginate(to_page, type)
	});

	$(document).on('click', '.search_shop', function(){
		var params = $('.search_input_shop').val();
		var type = $(this).attr('data-utype');
		if(params){
			paginate(1, type, params)
		}
	});

	function paginate(to_page, type, params = ''){
		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Categories/shop_paginate?type="+type,
			data 	: {
				'page' : to_page,
				'params' : params,
			},
			cache	: false,
			dataType : 'html',
			success : function(r){
				$('.products').html(r);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	}
})