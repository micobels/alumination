$(function(){
	$(document).on('click', '.login_btn', function(){
		var form = $('.login_form');

		var authen_form = {
			'email' : form.find('.email_add').val(),
			'password' : form.find('.acc_pass').val(),
		}

		if(authen_form['email'] == '' && authen_form['password'] == ''){
			$('.error_notify').find('p').text('Please fill up all the blank fields.');
			$('.error_notify').show();
			setTimeout(function(){
				$('.error_notify').fadeOut();
			},3000);
			return false;
		}

		$.ajax({
			type  	: 'POST',
			url 	: baseUrl+"Login/authenticate",
			data 	: {
				'info' : authen_form
			},
			cache	: false,
			dataType : 'json',
			success : function(r){
				console.log(r);
				if(r.status == 'success'){
					$('.success_notify').find('p').text('Logged In Successfully.');
					$('.success_notify').show();
					setTimeout(function(){
						$('.success_notify').fadeOut();
						window.location.reload();
					},3000);
				}else{
					$('.error_notify').find('p').text(r.message);
					$('.error_notify').show();
					setTimeout(function(){
						$('.error_notify').fadeOut();
					},3000);
				}
			},
			error 	: function(e){
				console.log('error');
			}
		})
	});

	$(document).on('change','#user_type',function(){
		console.log($(this).val());
		if($(this).val() == 2){
			$('.business_fields').slideDown();
		}else{
			$('.business_fields').slideUp();
		}
	})

	$(document).on('click', '.home_register_btn', function(){
		var form = $('.home_reg_form');
		var registration_data = {
			'fname' 			: form.find('#f_name').val(),
			'mname' 			: form.find('#m_name').val(),
			'lname' 			: form.find('#l_name').val(),
			'usertype' 			: form.find('#user_type').val(),
			'email' 			: form.find('#user_email').val(),
			'mobile' 			: form.find('#user_mobile').val(),
			'address_no' 		: form.find('#user_addr_no').val(),
			'address_barangay' 	: form.find('#user_addr_barangay').val(),
			'address_city' 		: form.find('#user_addr_city').val(),
			'address_state' 	: form.find('#user_addr_state').val(),
			'password' 			: form.find('#user_password').val(),
			'business_name' 	: form.find('#user_business_name').val(),
			'business_address' 	: form.find('#user_business_address').val(),
		}

		if(registration_data['usertype'] == 0){
			$('.error_notify').find('p').text('Please Select a Usertype.');
			$('.error_notify').show();
			setTimeout(function(){
				$('.error_notify').fadeOut();
			},3000);
			return false;
		}

		if((registration_data['password'] != form.find('#confirm_password').val()) || registration_data['password'] == "" || form.find('#confirm_password').val() == "" ){
			// console.log("password do not match");
			$('.error_notify').find('p').text('Password does not Match.');
			$('.error_notify').show();
			setTimeout(function(){
				$('.error_notify').fadeOut();
			},3000);
			return false;
		}
		proceed = true;
		$('.validate').each(function(){
			if($(this).is(':visible') && $(this).val() == ''){
				$('.error_notify').find('p').text('Please fill up all the blank fields.');
				$('.error_notify').show();
				setTimeout(function(){
					$('.error_notify').fadeOut();
				},3000);
				proceed = false;
				return false;
			}
		})
		if(proceed){		
			$.ajax({
				type  	: 'POST',
				url 	: baseUrl+"Register/register_account",
				data 	: {
					'info' : registration_data
				},
				cache	: false,
				dataType : 'json',
				success : function(r){
					$('.success_notify').find('p').text('Account Created Successfully.');
					$('.success_notify').show();
					setTimeout(function(){
						$('.success_notify').fadeOut();
						if(form.find('#user_type').val() == 2){
							window.location.href = baseUrl+"Registration_payment";
						}else{
							window.location.href = baseUrl + 'Login';
						}
					},3000);
				},
				error 	: function(e){
					console.log('error');
				}
			})
		}
	})
})

