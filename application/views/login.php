<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login - Alumination PH</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sublime project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap4/bootstrap.min.css">
    <link href="<?=base_url()?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <?php if(isset($css_array)):?>
        <?php foreach ($css_array as $key => $path):?>
            <link rel="stylesheet" type="text/css" href="<?=$path?>">
        <?php endforeach;?>
    <?php endif;?>
</head>
<style type="text/css">
    .color_top{
        background: #000;
        height: 200px;
        width: 200px;
        position: fixed;
        top: 0;
        left: 0;
        border-radius: 0 0 365px 0;
    }
    .color_bottom{
        background: #e95a5a;
        height: 200px;
        width: 200px;
        position: fixed;
        bottom: 0;
        right: 0;
        border-radius: 365px 0 0 0;
    }
    .mid_container{
        background: #fff;
        width: 350px;
        height: 500px;
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        border-radius: 10px;
        box-shadow: 0 0 20px 1px #d3d3d3;
    }
    .upper_level{
        background: url(assets/img/login.jpg)center center no-repeat;
        background-size: cover;
        height: 450px;
        border-radius: 10px 10px 0 0;
        position: relative;
    }
    .gradient_color{
        background-image: linear-gradient(to bottom left,#000,#e95a5a);
        height: 450px;
        opacity: 0.7;
        border-radius: 10px 10px 0 0;
    }
    .email_add{
        border: solid 2px #fff;
        outline: none;
        background: transparent;
        padding: 5px;
        width: 80%;
        margin-top: 60px;
        color: #fff;
        border-radius: 10px;
        text-align: center;
    }
    .acc_pass{
        border: solid 2px #fff;
        outline: none;
        background: transparent;
        padding: 5px;
        width: 80%;
        color: #fff;
        text-align: center;
        border-radius: 10px;
        margin-top: 10px;
    }
    .form_holder{
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        padding: 80px 20px 20px;
        text-align: center;
        color: #fff;
    }
    .form_holder button{
        width: 80%;
        border: solid 2px #fff;
        color: #fff;
        background: transparent;
        transition: all ease-in-out 0.3s;
        cursor: pointer;
        margin-top: 50px;
        padding: 10px;
    }
    .form_holder button:hover{
        background: #fff;
        color: #000;
    }
    input::placeholder{
        color: #fff;
    }
    .lower_level{
        text-align: center;
        color: #555!important;
        padding-top: 10px;
    }
    .success_notify,
    .error_notify{
        background: #96e194;
        position: fixed;
        right: 20px;
        bottom: 20px;
        padding: 10px;
        box-shadow: 0px 0px 5px 1px #74be74;
        animation: show_notif ease-in-out 1.3s;
        display: none;
        z-index: 10;
    }
    .error_notify{
        background: #e19494;
        box-shadow: 0px 0px 5px 1px #be7474;
    }
    .success_notify .row i{
        font-size: 25px;
        color: #347f34;
    }
    .error_notify .row i{
        font-size: 25px;
        color: #7f3434;
    }
    .success_notify .remove_notif,
    .error_notify .remove_notif{
        position: absolute;
        top: -10px;
        right: -10px;
        padding: 5px 6px;
        background: #f5f5f5;
        color: #000;
        border-radius: 20px;
        cursor: pointer;
        box-shadow: 0 0 1px 1px #96e194;
    }
    .error_notify .remove_notif{
        box-shadow: 0px 0px 5px 1px #be7474;
    }
    .success_notify p,
    .error_notify p{
        color: #000;
    }
    @keyframes show_notif{
        0%{
            opacity: 0;
            bottom: 0;
        }
        50%{
            opacity: 1;
            bottom: 30px;
        }
        100%{
            opacity: 1;
            bottom: 20px;
        }
    }
</style>

<body style="background: #f5f5f5;">
    <div class="color_top">
    </div>
    <div class="color_bottom">
    </div>

    <div class="mid_container">
        <div class="upper_level">
            <div class="gradient_color"></div>
            <div class="form_holder">
                <h3 style="font-weight: 700;">Alumination <span style="color: #e95a5a;">PH</span></h3>
                <form class="login_form">
                    <input type="email" class="email_add" placeholder="Email Address">
                    <input type="password" class="acc_pass" placeholder="Password">
                    <button type="button" class="login_btn">Login</button>
                </form>
            </div>
        </div>
        <div class="lower_level">
            <small><a href="<?=base_url()?>Register">Create an Account</a></small>
        </div>
    </div>

    <div class="success_notify">
        <div class="row">
            <div class="col-sm-2">
                <i class="fa fa-check"></i>
            </div>
            <div class="col-sm-10">
                <p></p>
            </div>
        </div>
        <i class="fa fa-close remove_notif"></i>
    </div>

    <div class="error_notify">
        <div class="row">
            <div class="col-sm-2">
                <i class="fa fa-warning"></i>
            </div>
            <div class="col-sm-10">
                <p></p>
            </div>
        </div>
        <i class="fa fa-close remove_notif"></i>
    </div>
</body>

<?php foreach ($js_array as $key => $js_val):?>
    <script src="<?=$js_val?>"></script>
<?php endforeach;?>
<script>
    var baseUrl = "<?=base_url()?>"
</script>

</html>