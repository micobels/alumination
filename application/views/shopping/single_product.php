<style type="text/css">
	#upload_book_receipt ul{
		display: flex;
	}
	#upload_book_receipt ul li{
		width: 100%;
		margin: auto;
	}
	#upload_book_receipt ul li img{
		width: 100%;
		padding: 10px;
		margin: auto;
	}
	#upload_book_receipt .preview_area img{
		margin: 20px;
		border: solid #f5f5f5 3px;
		width: 90%;
		height: 40vh;
		object-fit: contain;
		padding: 10px;
	}
	.seller_information{
		border: solid 2px #e95a5a;
	    padding: 10px;
	    text-align: center;
	    box-shadow: 0 0 20px 3px #d3d3d3;
	}
</style>
<div class="product_details" style="margin-top: 150px;">
	<div class="container">
		<div class="row details_row">

			<!-- Product Image -->
			<div class="col-lg-6">
				<div class="details_image">
					<div class="details_image_large"><img src="<?=base_url().json_decode($item['images'])[0]?>" alt=""></div>
					<div class="details_image_thumbnails d-flex flex-row align-items-start justify-content-between">
						<?php foreach (json_decode($item['images']) as $key => $value): ?>
							<?php if($key == 0): ?>
								<div class="details_image_thumbnail active" data-image="<?=base_url().$value?>"><img src="<?=base_url().$value?>" alt=""></div>
							<?php else: ?>
								<div class="details_image_thumbnail" data-image="<?=base_url().$value?>"><img src="<?=base_url().$value?>" alt=""></div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<!-- Product Content -->
			<div class="col-lg-6">
				<div class="details_content">
					<div class="details_name"><?=$item['name']?></div>
					<div class="details_text">
						<p><?=substr($item['description'],0,200); ?></p>
					</div>

					<div class="product_quantity_container">
						<?php if(isset($user)):?>
							<?php if($user['usertype'] == 3) : ?>
								<?php if(isset($request)): ?>
									<?php if($request['status'] == 2): ?>
										<div class="seller_information">
											<h4 style="color: #000;">Seller's Information</h4>
											<p style="margin: 0;"><b>Name</b>: <?=$seller['fname'].' '.$seller['mname'].' '.$seller['lname'] ?></p>
											<p style="margin: 0;"><b>Email</b>: <?=$seller['email']?></p>
											<p style="margin: 0;"><b>Contact Number</b>: <?=$seller['mobile']?></p>
										</div>
									<?php else: ?>
										<p>Please wait while the Admin Verfies your Transaction.</p>
									<?php endif; ?>
								<?php else: ?>
									<div class="button cart_button upload_resibo" data-id="<?=$item['pid']?>"><a href="#">Get Details</a></div>
								<?php endif; ?>
							<!-- <div class="button cart_button approved_book"><a href="#">Get Details</a></div> -->
							<?php endif; ?>
						<?php else:?>
							<div class="button cart_button redirect_login"><a href="#">Get Details</a></div>
						<?php endif; ?>
					</div>

					<div class="details_share">
						<span>Share:</span>
						<ul>
							<li>
								<a target="_blank" href="https://plus.google.com/share?url=<?=base_url()?>Item/info/<?=$item['pid']?>">
									<i class="fa fa-google-plus" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url()?>Item/info/<?=$item['pid']?>">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://twitter.com/home?status=<?=base_url()?>Item/info/<?=$item['pid']?>">
									<i class="fa fa-twitter" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="row description_row">
			<div class="col">
				<div class="description_title_container">
					<div class="description_title">Description</div>
					<!-- <div class="reviews_title"><a href="#">Reviews <span>(1)</span></a></div> -->
				</div>
				<div class="description_text">
					<p><?=$item['description']?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Products -->

<div class="products">
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<div class="products_title">Related Products</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				
				<div class="product_grid">

					<?php foreach ($products as $key => $prod): ?>
					<div class="product">
						<div class="product_image"><img src="<?=base_url().json_decode($prod['images'])[0]?>" alt=""></div>
						<div class="product_content">
							<div class="product_title"><a href="<?=base_url()?>Item/info/<?=$prod['pid']?>"><?=$prod['name']?></a></div>
							<div class="product_price"><?=substr($prod['description'],0,50)?>...</div>
						</div>
					</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div id="book_service_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Book this Service</h4>
      		</div>
      		<div class="modal-body text-center">
      			<p>Are you sure you want to book this service?</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-success book_service" data-id="<?=$item['pid']?>">Book Service</button>
      		</div>
    	</div>
  	</div>
</div> -->

<div id="login_first" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Login First!</h4>
      		</div>
      		<div class="modal-body text-center">
      			<p>Before you can Proceed in Booking some of our services, You must login First so we can use your Personal Information Listed in your Account.</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<a href="<?=base_url()?>Login">
        			<button type="button" class="btn btn-secondary login_redirect">Login</button>
        		</a>
      		</div>
    	</div>
  	</div>
</div>

<!-- <div id="success_booking" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Successfully Booked Service!</h4>
      		</div>
      		<div class="modal-body text-center">
      			<p>You Have Successfully Pre Booked a Product. Please Proceed to '<a href="<?=base_url()?>Booked_services">My Booked Services</a>' Section and Upload your Receipt to Successfully Book the Service.</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div> -->

<div id="success_booking" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Notification</h4>
      		</div>
      		<div class="modal-body text-center">
      			<p>Please Wait thile our Admin verify your transaction, Once Verified, you can visit this product and be able to see the details of the Seller.</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="upload_book_receipt" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<form method="POST" action="#" enctype="multipart/form-data" class="upload_reciept_form" id="upload_reciept_form">
		  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
		    		<h4 class="modal-title">Upload Receipt</h4>
		  		</div>
		  		<div class="modal-body text-center">
		  			<p>In order to <b>Unlock</b> the information of the seller, please pay 100.00</p>
		  			<p>The following are the Banks Credited for this Transaction.</p>
		  			<ul class="list-inline">
		  				<li><img src="<?=base_url()?>assets/img/bank/bdo.png"><br /><div>0123456789874</div></li>
		  				<li><img src="<?=base_url()?>assets/img/bank/bpi.png"><br /><div>0123456789874</div></li>
		  				<li><img src="<?=base_url()?>assets/img/bank/metro.jpg"><br /><div>0123456789874</div></li>
		  			</ul>
		      			<div class="form-group">
			      			<div class="preview_area">
			      			</div>
							<label class="upload_btn newsletter_button trans_200" for="upload_reciept_image" style="color: #fff;"><span style="margin-top: 15px;">Upload Image</span></label>
							<input type="file" id="upload_reciept_image" name="reciept_image" accept="image/*" style="display: none;">
						</div>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    		<button type="submit" class="btn btn-success upload_proceed">Upload Receipt</button>
		  		</div>
			</form>
    	</div>
  	</div>
</div>