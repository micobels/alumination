<style type="text/css">
	.newsletter{
		display: none;
	}
	.numbers{
		background: #e95a5a;
	    color: #fff;
	    border-radius: 50px;
	    margin: 10px auto;
	    display: inline-block;
        width: 30px;
	    height: 30px;
	    padding: 5px;
	    text-align: center;
	}
	.icon_box_title{
	    font-size: 18px;
	    font-weight: 500;
	    color: #1b1b1b;
	    margin-top: 12px;
	}
</style>
<div class="home">
	<div class="home_container">
		<div class="home_background" style="background-image:url(assets/img/contact.jpg)"></div>
		<div class="home_content_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="breadcrumbs">
								<ul>
									<li><a href="<?=base_url();?>">Home</a></li>
									<li>Registration Payment</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	

<div class="icon_boxes">
	<div class="container">
		<div class="row icon_box_row">

			<div class="col-sm-7">
				<h4>How to Activate your Account?</h4>
				<div class="row">
					<div class="col-sm-1">
						<span class="numbers">1</span>
					</div>
					<div class="col-sm-10">
						<div class="icon_box_title">Payment for Registration</div>
						<div class="icon_box_text">
							<p>In order to activate your account, you must pay 200.00 as a registration Fee. This will fund up the Maintenance of the System and your 24/7 Customer Service for Seller and Customer.</p>
							<p>Please Pay Only to the Following Banks and Accounts:</p>
							<ul class="list-inline" style="display: flex;">
								<li style="width: 100%;height: 20vh;text-align: center;">
									<div class="bank_details_container" style="position: relative;padding: 30px;height: 100%;display: flex;">
										<img src="assets/img/bank/bdo.png" style="width: 100%;margin: auto;">
									</div>
									<span>7896520578638623</span>
								</li>
								<li style="width: 100%;height: 20vh;text-align: center;">
									<div class="bank_details_container" style="position: relative;padding: 30px;height: 100%;display: flex;">
										<img src="assets/img/bank/bpi.png" style="width: 100%;margin: auto;">
									</div>
									<span>7896520578638623</span>
								</li>
								<li style="width: 100%;height: 20vh;text-align: center;">
									<div class="bank_details_container" style="position: relative;padding: 30px;height: 100%;display: flex;">
										<img src="assets/img/bank/metro.jpg" style="width: 100%;margin: auto;">
									</div>
									<span>7896520578638623</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row" style="margin-top: 50px;">
					<div class="col-sm-1">
						<span class="numbers">2</span>
					</div>
					<div class="col-sm-10">
						<div class="icon_box_title">Fill Up the Form</div>
						<div class="icon_box_text">
							<p>Provide all the Necessary Information Needed in the Form located at the Right side of your Screen.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-5">
				<div class="form_container" style="height: 550px;width: 100%;border: solid 1px #d3d3d3;box-shadow: 0 0 5px #d3d3d3;">
					<h3 style="text-align: center;margin-top: 50px;color: #000;"><b>Account Activation Form</b></h3>
					<form style="padding: 30px;" method="POST" action="#" enctype="multipart/form-data" class="submit_reg_receipt" id="submit_reg_receipt">
						<div class="form-group">
						    <label for="email_address" style="color: #e95a5a;">Email address</label>
						    <input type="email" class="form-control" id="email_address" aria-describedby="emailHelp" placeholder="Enter Email">
						    <small id="email_address" class="form-text text-muted">Email of the Account you've Registered.</small>
					  	</div>
					  	<div class="form-group">
						    <label for="password" style="color: #e95a5a;">Account Password</label>
						    <input type="password" class="form-control" id="password" placeholder="Enter Password">
						    <small id="password" class="form-text text-muted">Password of the Registered Account.</small>
					  	</div>
					  	<div class="form-group">
						    <label for="receipt_upload" style="color: #e95a5a;">Upload Receipt of Payment</label>
						    <input type="file" class="form-control-file" name="acc_receipt" id="receipt_upload">
						    <small id="receipt_upload" class="form-text text-muted">Upload an Image of your Payment Receipt.</small>
					  	</div>

					  	<button type="submit" class="newsletter_button trans_200" style="margin: 40px auto 0;display: block;"><span>Validate</span></button>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>