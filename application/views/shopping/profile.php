<style type="text/css">
.form-control{
	color: #999;
}
.image_upload_preview ul{
	display: flex;
}
.image_upload_preview li{
	width: 10%;
	margin: 5px;
	border: solid 2px #d3d3d3;
}
.image_upload_preview li:first-child{
	margin-left: 0;
}
.image_upload_preview li img{
	width: 100%;
	height: 120px;
	object-fit: cover;
}
.white_container{
    margin: 150px 0 -50px;
    background: #fff;
    box-shadow: 0px 0px 3px #d3d3d3;
    z-index: 3;
}
.white_container .title_holder{
    padding: 10px 20px;
    background: #383838;
}
.white_container .title_holder h3{
    padding: 0;
    margin: 0;
    color: #fff;
}
.white_container .title_holder i,
.white_container label{
    color: #e95a5a;
}
.white_container .body_container{
    padding: 10px 20px;
}
</style>
<div style="padding: 20px;z-index: 3;background: #fff;">
	<div class="white_container">
		<div class="title_holder"> 
			<h3><i class="fa fa-user"></i> Edit Profile</h3>
		</div>
		<div class="body_container">
			<form class="admin_registration_form">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="user_name">First Name</label>
							<input type="text" class="form-control validate" id="user_fname" value="<?=$user_data['fname']?>" placeholder="First Name">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label for="user_name">Middle Name</label>
							<input type="text" class="form-control validate" id="user_mname" value="<?=$user_data['mname']?>" placeholder="Middle Name">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label for="user_name">Last Name</label>
							<input type="text" class="form-control validate" id="user_lname" value="<?=$user_data['lname']?>" placeholder="Last Name">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="user_email">Email Address</label>
							<input type="email" class="form-control validate" value="<?=$user_data['email']?>" id="user_email" placeholder="User Email">
							<small class="form-text text-muted">Your Email will be your Username.</small>
						</div>
					</div>
					<?php if(isset($user_data['mobile'])):?>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="user_mobile">Mobile Number</label>
								<input type="number" class="form-control validate" id="user_mobile" value="<?=$user_data['mobile']?>" placeholder="User Mobile Number">
							</div>
						</div>
					<?php endif;?>
				</div>
				<?php if($user_data['usertype'] == 2): ?>
				<div class="row business_info_container">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="user_business_name">Business Name</label>
							<input type="email" class="form-control validate" value="<?=$user_data['business_name']?>" id="user_business_name" placeholder="Business Name">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="user_business_address">Business Address</label>
							<input type="text" class="form-control validate" id="user_business_address" value="<?=$user_data['business_address']?>" placeholder="Business Addressr">
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if(isset($user_data['address'])):?>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="user_address">Address</label>
								<input type="text" class="form-control validate" id="user_address" value="<?=$user_data['address']?>" placeholder="User Official Address">
							</div>
						</div>
					</div>
				<?php endif;?>

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="user_email">New Password</label>
							<input type="password" class="form-control validate_pass" id="user_pass" placeholder="New Password">
							<small class="form-text text-muted">Your Password wont be Changed if you dont fill this Field.</small>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="user_mobile">Confirm Password</label>
							<input type="password" class="form-control validate_pass" id="confirm_pass" placeholder="Confirm Password">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-right">
						<div class="form-group">
							<button type="button" class="btn btn-primary proceed_edits">Edit Account</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>