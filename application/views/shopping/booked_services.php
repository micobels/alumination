<style type="text/css">
	.white_container{
        margin: 150px 20px 50px;
        background: #fff;
        box-shadow: 0px 0px 3px #d3d3d3;
        z-index: 3;
    }
    .white_container .title_holder{
        padding: 10px 20px;
        background: #383838;
    }
    .white_container .title_holder h3{
        padding: 0;
        margin: 0;
        color: #fff;
    }
    .white_container .title_holder i,
    .white_container label{
        color: #e95a5a;
    }
    .white_container .body_container{
        padding: 10px 20px;
    }
	table thead th{
		color: #e95a5a;
	}
	table tbody tr{
		color: #000;
	}
	.pagination{
		margin-top: 30px;
		color: #000;
		font-size: 15px;
	}
	.pagination i{
		margin: auto 20px;
		cursor: pointer;
		font-size: 25px;
	}
	.pagination i:hover{
		color: #e95a5a;
	}
	#upload_images ul{
		display: flex;
	}
	#upload_images ul li{
		width: 100%;
		margin: auto;
	}
	#upload_images ul li img{
		width: 100%;
		padding: 10px;
		margin: auto;
	}
	#upload_images .preview_area img{
		margin: 20px;
		border: solid #f5f5f5 3px;
		width: 90%;
		height: 40vh;
		object-fit: contain;
		padding: 10px;
	}
	.overlay_container{
		background: #fff;
		z-index: 3;
		display: inline-block;
		width: 100%;
	}
</style>
<div class="overlay_container">
	<div class="white_container">
		<div class="title_holder"> 
			<h3><i class="fa fa-users"></i> Account Lists</h3>
		</div>
		<div class="body_container">
			<div class="data_container">
				<table class="table table-hover text-center">
			  		<thead>
			    		<tr>
			      			<th scope="col">Request ID</th>
			      			<th scope="col">Service Name</th>
			      			<th scope="col">Date</th>
			      			<th scope="col">Status</th>
			      			<th scope="col">Action</th>
			    		</tr>
			  		</thead>
			  		<tbody class="account_list_table">
			  			<?php foreach ($requests as $key => $req): ?>
				    		<tr>
				      			<td><?=$req['rid']?></td>
				      			<td><?=$products[$req['pid']]['name']?></td>
				      			<td><?=$req['create_date']?></td>
				      			<td><?=$status[$req['status']]?></td>
				      			<td>
				      				<?php if($req['status'] == 0) : ?>
				      					<button class="btn btn-success btn-sm upload_resibo" data-id="<?=$req['rid']?>"><i class="fa fa-upload"></i></button>
				      					<button class="btn btn-danger btn-sm cancel_service" data-id="<?=$req['rid']?>"><i class="fa fa-ban"></i></button>
				      				<?php elseif($req['status'] == 1 ||$req['status'] == 2): ?>
				      					<button class="btn btn-primary btn-sm customer_support" data-id="<?=$req['rid']?>"><i class="fa fa-comments"></i></button>
				      					<button class="btn btn-danger btn-sm cancel_service" data-id="<?=$req['rid']?>"><i class="fa fa-ban"></i></button>
			      					<?php elseif($req['status'] == 3): ?>
				      					<button class="btn btn-warning btn-sm open_feedback" data-id="<?=$req['rid']?>"><i class="fa fa-star"></i></button>
				      				<?php elseif($req['status'] == 4): ?>
				      					<button class="btn btn-primary btn-sm uncancel" data-id="<?=$req['rid']?>"><i class="fa fa-refresh"></i></button>
				      				<?php endif; ?>
				      			</td>
				    		</tr>
				    	<?php endforeach;?>
			  		</tbody>
				</table>

				<div class="pagination account_list_pagination" data-type="customer_services">
					<div class="paginate prev_page">
						<i class="fa fa-angle-left"></i>
					</div>
					<div class="details">
						<span class="cur_page">1</span> of <span class="max_page"><?=$pages?></span>
					</div>
					<div class="paginate next_page">
						<i class="fa fa-angle-right"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="upload_images" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<form method="POST" action="#" enctype="multipart/form-data" class="upload_reciept_form" id="upload_reciept_form">
		  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
		    		<h4 class="modal-title">Upload Receipt</h4>
		  		</div>
		  		<div class="modal-body text-center">
		  			<p>The following are the Banks Credited for this Transaction.</p>
		  			<ul class="list-inline">
		  				<li><img src="<?=base_url()?>assets/img/bank/bdo.png"><br /><div>0123456789874</div></li>
		  				<li><img src="<?=base_url()?>assets/img/bank/bpi.png"><br /><div>0123456789874</div></li>
		  				<li><img src="<?=base_url()?>assets/img/bank/metro.jpg"><br /><div>0123456789874</div></li>
		  			</ul>
		      			<div class="form-group">
			      			<div class="preview_area">
			      			</div>
							<label class="upload_btn newsletter_button trans_200" for="upload_reciept_image" style="color: #fff;"><span style="margin-top: 15px;">Upload Image</span></label>
							<input type="file" id="upload_reciept_image" name="reciept_image" accept="image/*" style="display: none;">
						</div>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    		<button type="submit" class="btn btn-success upload_proceed">Upload Receipt</button>
		  		</div>
			</form>
    	</div>
  	</div>
</div>

<div id="success_upload" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<form method="POST" action="#" enctype="multipart/form-data" class="upload_reciept_form" id="upload_reciept_form">
		  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
		    		<h4 class="modal-title">Upload Receipt Success</h4>
		  		</div>
		  		<div class="modal-body text-center">
		  			<p>Payment Receipt Uploaded Successfully. Please Wait for the Admin to confirm the Transaction.</p>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  		</div>
			</form>
    	</div>
  	</div>
</div>

<div id="message_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<form method="POST" action="#" enctype="multipart/form-data" class="upload_reciept_form" id="upload_reciept_form">
		  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
		    		<h4 class="modal-title">Customer Support Chat Service</h4>
		  		</div>
		  		<div class="modal-body text-center">
		  			<p>Please Wait while we Verify/process your Order. You can Contact our Customer Service Using messenger (Facebook.com) or Click <a href="#">Here</a>.</p>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  		</div>
			</form>
    	</div>
  	</div>
</div>

<div id="cancel_confirmation" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<form method="POST" action="#" enctype="multipart/form-data" class="upload_reciept_form" id="upload_reciept_form">
		  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
		    		<h4 class="modal-title">Confirmation</h4>
		  		</div>
		  		<div class="modal-body text-center">
		  			<p>Are your sure you want to cancel this service?</p>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    		<button type="button" class="btn btn-danger proceed_cancelling">Proceed</button>
		  		</div>
			</form>
    	</div>
  	</div>
</div>

<div id="uncancel_confirmation" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<form method="POST" action="#" enctype="multipart/form-data" class="upload_reciept_form" id="upload_reciept_form">
		  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
		    		<h4 class="modal-title">Confirmation</h4>
		  		</div>
		  		<div class="modal-body text-center">
		  			<p>Are your sure you want to Continue with this service again?</p>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    		<button type="button" class="btn btn-primary proceed_uncancelling">Proceed</button>
		  		</div>
			</form>
    	</div>
  	</div>
</div>

<div id="add_feedback" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<form method="POST" action="#" enctype="multipart/form-data" class="upload_reciept_form" id="upload_reciept_form">
		  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
		    		<h4 class="modal-title">Place your Feedback</h4>
		  		</div>
		  		<div class="modal-body text-center">
		  			<p>You can Place Feedbacks from the Service Provided. It would be a great help for us to Improve what needs improving.</p>
		  			<textarea rows="5" style="width: 100%;padding: 10px;" placeholder="Place Your Feedback Here."></textarea>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    		<button type="button" class="btn btn-success save_feedback">Submit</button>
		  		</div>
			</form>
    	</div>
  	</div>
</div>