<style type="text/css">
	.newsletter{
		display: none;
	}
	.numbers{
		background: #e95a5a;
	    color: #fff;
	    border-radius: 50px;
	    margin: 10px auto;
	    display: inline-block;
        width: 30px;
	    height: 30px;
	    padding: 5px;
	}
</style>
<div class="home">
	<div class="home_container">
		<div class="home_background" style="background-image:url(assets/img/contact.jpg)"></div>
		<div class="home_content_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="breadcrumbs">
								<ul>
									<li><a href="<?=base_url();?>">Home</a></li>
									<li>How to be A Seller</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	

<div class="icon_boxes">
	<div class="container">
		<div class="row icon_box_row">
			
			<!-- Icon Box -->
			<div class="col-lg-4 icon_box_col">
				<div class="icon_box">
					<span class="numbers">1</span>
					<div class="icon_box_image"><img src="assets/img/reg.png" alt=""></div>
					<div class="icon_box_title">Create Selling Account</div>
					<div class="icon_box_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie.<a href="<?=base_url()?>Register">Here</a></p>
					</div>
				</div>
			</div>

			<!-- Icon Box -->
			<div class="col-lg-4 icon_box_col">
				<div class="icon_box">
					<span class="numbers">2</span>
					<div class="icon_box_image"><img src="assets/img/product.png" alt=""></div>
					<div class="icon_box_title">Register A Product</div>
					<div class="icon_box_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie.</p>
					</div>
				</div>
			</div>

			<!-- Icon Box -->
			<div class="col-lg-4 icon_box_col">
				<div class="icon_box">
					<span class="numbers">3</span>
					<div class="icon_box_image"><img src="assets/img/bill.png" alt=""></div>
					<div class="icon_box_title">Entertain Customers</div>
					<div class="icon_box_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie.</p>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>