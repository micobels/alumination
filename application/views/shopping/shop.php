<!-- Products -->
<style type="text/css">
	.sorting_bar{
		position: relative;
	}
	.search_container{
		position: absolute;
		right: 0;
	}
	.search_container input{
		padding: 10px;
	    outline: none;
	    border: none;
	    border-bottom: solid 2px #d3d3d3;
	    width: 300px;
	    transition: all ease-in-out 0.3s;
	}
	.search_container input:focus{
		border-color: #e95a5a;
	}
	.search_container button{
		background: #fff;
	    border: none;
	    font-size: 20px;
	    outline: none;
	    cursor: pointer;
	    color: #000;
	    transition: all ease-in-out 0.3s;
	}
	.search_container button:hover{
		color: #e95a5a;
	}
</style>

<div class="products">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="sorting_bar d-flex flex-md-row flex-column align-items-md-center justify-content-md-start">
					<div class="results">Showing <span>12</span> results</div>
					<div class="search_container">
						<input type="text" class="search_input_shop" placeholder="Search Product Here.">
						<button type="button" class="search_shop" data-utype="<?=isset($_GET['type']) ? $_GET['type'] : ''?>"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="product_grid">
					<?php foreach ($products as $key => $prod): ?>
					<div class="product">
						<div class="product_image"><img src="<?=base_url().json_decode($prod['images'])[0]?>" alt=""></div>
						<div class="product_content">
							<div class="product_title"><a href="<?=base_url()?>Item/info/<?=$prod['pid']?>"><?=$prod['name']?></a></div>
							<div class="product_price"><?=substr($prod['description'],0,50)?>...</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="product_pagination" style="margin-left: 20px;">
				<ul>
					<li class="page prev_page" data-utype="<?=isset($_GET['type']) ? $_GET['type'] : ''?>"><<</li>
					<li class="active_page">1</li> of <li class="page_total"><?=$products_pages?></li>
					<li class="page next_page" data-utype="<?=isset($_GET['type']) ? $_GET['type'] : ''?>">>></li>
				</ul>
			</div>
		</div>
	</div>
</div>