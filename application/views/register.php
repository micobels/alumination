<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register - Alumination PH</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sublime project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap4/bootstrap.min.css">
    <link href="<?=base_url()?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <?php if(isset($css_array)):?>
        <?php foreach ($css_array as $key => $path):?>
            <link rel="stylesheet" type="text/css" href="<?=$path?>">
        <?php endforeach;?>
    <?php endif;?>
</head>
<style type="text/css">
    .color_top{
        background: #000;
        height: 200px;
        width: 200px;
        position: fixed;
        bottom: 0;
        left: 0;
        border-radius: 0 365px 0 0;
    }
    .color_bottom{
        background: #e95a5a;
        height: 200px;
        width: 200px;
        position: fixed;
        top: 0;
        right: 0;
        border-radius: 0 0 0 365px;
    }
    .mid_container{
        background: #fff;
        width: 65%;
        max-height: 645px;
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        border-radius: 10px;
        box-shadow: 0 0 20px 1px #d3d3d3;
    }
    .left_level{
        background: url(assets/img/login.jpg)center center no-repeat;
        background-size: cover;
        max-height: 645px;
        border-radius: 10px 0 0 10px;
        position: relative;
        padding: 0;
    }
    .gradient_color{
        background-image: linear-gradient(to bottom left,#000,#e95a5a);
        height: calc(100.5%);
        opacity: 0.7;
        border-radius: 10px 0 0 10px;
    }
    .mid_container .left_level h2{
        position: absolute;
        top: 50px;
        right: 5px;
        color: #fff;
        font-weight: 700;
        font-size: 20px;
    }
    .right_level{
        padding: 0;
    }
    .form_container{
        padding: 10px;
    }
    .form_container #f_name,
    .form_container #m_name,
    .form_container #l_name,
    .form_container #user_address,
    .form_container #user_email{
        width: 96%;
        font-size: 13px;
    } 
    .form_container #user_mobile{
        font-size: 13px;
    } 
    .form_container #user_type{
        width: 90%;
        font-size: 13px;
        height: 33px;
    }
    .form_container button{
        background: #fff;
        border: solid 1px #e95a5a;
        color: #e95a5a;
        padding: 10px;
        margin: auto;
        display: block;
        width: 50%;
        cursor: pointer;
        transition: all ease-in-out 0.3s;
    }
    .form_container button:hover{
        background: #e95a5a;
        color: #fff;
    }
    #user_business_name, #user_business_address{
        width: 95% !important;
    }
    .success_notify,
    .error_notify{
        background: #96e194;
        position: fixed;
        right: 20px;
        bottom: 20px;
        padding: 10px;
        box-shadow: 0px 0px 5px 1px #74be74;
        animation: show_notif ease-in-out 1.3s;
        display: none;
        z-index: 10;
    }
    .error_notify{
        background: #e19494;
        box-shadow: 0px 0px 5px 1px #be7474;
    }
    .success_notify .row i{
        font-size: 25px;
        color: #347f34;
    }
    .error_notify .row i{
        font-size: 25px;
        color: #7f3434;
    }
    .success_notify .remove_notif,
    .error_notify .remove_notif{
        position: absolute;
        top: -10px;
        right: -10px;
        padding: 5px 6px;
        background: #f5f5f5;
        color: #000;
        border-radius: 20px;
        cursor: pointer;
        box-shadow: 0 0 1px 1px #96e194;
    }
    .error_notify .remove_notif{
        box-shadow: 0px 0px 5px 1px #be7474;
    }
    .success_notify p,
    .error_notify p{
        color: #000;
    }
    @keyframes show_notif{
        0%{
            opacity: 0;
            bottom: 0;
        }
        50%{
            opacity: 1;
            bottom: 30px;
        }
        100%{
            opacity: 1;
            bottom: 20px;
        }
    }
    .business_fields{
        display: none;
    }
</style>

<body style="background: #f5f5f5;">
    <div class="color_top">
    </div>
    <div class="color_bottom">
    </div>

    <div class="mid_container">
        <div class="row">
            <div class="col-sm-3 left_level">
                <div class="gradient_color"></div>
                <h2>Alumination <span style="color: #e95a5a;">PH</span></h2>
            </div>
            <div class="col-sm-9 right_level">
                <div class="form_container home_reg_form">
                    <form>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="user_name"><small>First Name</small></label>
                                    <input type="text" class="form-control validate" id="f_name" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="m_name"><small>Middle Name</small></label>
                                    <input type="text" class="form-control validate" id="m_name" placeholder="Middle Name">
                                </div>
                            </div>
                            <div class="col-sm-4" style="padding: 0;">
                                <div class="form-group">
                                    <label for="l_name"><small>Last Name</small></label>
                                    <input style="width: 85%;" type="text" class="form-control validate" id="l_name" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="user_mobile"><small>Mobile Number</small></label>
                                    <input type="number" class="form-control validate" id="user_mobile" placeholder="User Mobile Number">
                                </div>
                            </div>
                            <div class="col-sm-6" style="padding: 0;">
                                <div class="form-group">
                                    <label for="user_type"><small>User Type</small></label>
                                    <select class="form-control" id="user_type">
                                        <option value="0">Choose Type</option>
                                        <option value="2">Seller</option>
                                        <option value="3">Customer</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="width: 100%">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="user_address"><small>Number</small></label>
                                    <input type="text" class="form-control validate" id="user_addr_no" placeholder="House No.">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="user_address"><small>Barangay</small></label>
                                    <input type="text" class="form-control validate" id="user_addr_barangay" placeholder="Barangay">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="user_address"><small>City/Municipality</small></label>
                                    <input type="text" class="form-control validate" id="user_addr_city" placeholder="City/Municipality">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="user_address"><small>State</small></label>
                                    <input type="text" class="form-control validate" id="user_addr_state" placeholder="State">
                                </div>
                            </div>
                        </div>
                        <div class="business_fields">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="user_business_name"><small>Business Name</small></label>
                                        <input type="text" class="form-control validate" id="user_business_name" placeholder="Official Business Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="user_business_address"><small>Business Address</small></label>
                                        <input type="text" class="form-control validate" id="user_business_address" placeholder="Official Business Address">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="user_email"><small>Email</small></label>
                                    <input type="email" class="form-control validate" id="user_email" placeholder="Email Address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="user_password"><small>Password</small></label>
                                    <input type="password" class="form-control validate" id="user_password">
                                </div>
                            </div>
                            <div class="col-sm-6" style="padding: 0;">
                                <div class="form-group">
                                    <label for="confirm_password"><small>Confirm Password</small></label>
                                    <input type="password" class="form-control validate" id="confirm_password" style="width: 86%;">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="button" class="home_register_btn">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="success_notify">
        <div class="row">
            <div class="col-sm-2">
                <i class="fa fa-check"></i>
            </div>
            <div class="col-sm-10">
                <p></p>
            </div>
        </div>
        <i class="fa fa-close remove_notif"></i>
    </div>

    <div class="error_notify">
        <div class="row">
            <div class="col-sm-2">
                <i class="fa fa-warning"></i>
            </div>
            <div class="col-sm-10">
                <p></p>
            </div>
        </div>
        <i class="fa fa-close remove_notif"></i>
    </div>
</body>
<?php if(isset($js_array)):?>
    <?php foreach ($js_array as $key => $path):?>
        <script src="<?=$path?>" ></script>
    <?php endforeach;?>
<?php endif;?>
<script>
    var baseUrl = "<?=base_url()?>";
</script>
</html>