

<div class="container">
	<div class="row">
		<div class="col">
			<div class="sorting_bar d-flex flex-md-row flex-column align-items-md-center justify-content-md-start">
				<div class="results">Showing <span>12</span> results</div>
				<div class="search_container">
					<input type="text" class="search_input_shop" placeholder="Search Product Here.">
					<button type="button" class="search_shop" data-utype="<?=isset($_GET['type']) ? $_GET['type'] : ''?>"><i class="fa fa-search"></i></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="product_grid">
				<?php foreach ($products as $key => $prod): ?>
				<div class="product">
					<div class="product_image"><img src="<?=base_url().json_decode($prod['images'])[0]?>" alt=""></div>
					<div class="product_content">
						<div class="product_title"><a href="<?=base_url()?>Item/info/<?=$prod['pid']?>"><?=$prod['name']?></a></div>
						<div class="product_price"><?=substr($prod['description'],0,50)?>...</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="product_pagination" style="margin-left: 20px;">
			<ul>
				<li class="page prev_page" data-utype="<?=isset($_GET['type']) ? $_GET['type'] : ''?>"><<</li>
				<li class="active_page"><?=$active_page?></li> of <li class="page_total"><?=$products_pages?></li>
				<li class="page next_page" data-utype="<?=isset($_GET['type']) ? $_GET['type'] : ''?>">>></li>
			</ul>
		</div>
	</div>
</div>