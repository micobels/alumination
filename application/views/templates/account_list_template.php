<?php foreach ($list as $key => $account): ?>
	<tr>
		<td><?=$account['uid']?></td>
		<td style="text-align: left;">
			<b>Name:</b> <?=$account['fname'] ." ". $account['mname'] ." ".$account['lname']?> <br>
			<b>Email:</b> <?=$account['email']?> <br>
			<b>Mobile:</b> <?=isset($account['mobile']) ? $account['mobile'] : ""?>
		</td>
		<td><?=$reference[$account['usertype']]?></td>
		<td>
			<?php if($account['usertype'] == 2): ?>
				<?php if($account['status'] == 1): ?>
				<button class="btn btn-warning btn-sm activate_account" data-uid="<?=$account['uid']?>"><i class="fa fa-dashboard"></i></button>
				<?php endif; ?>
			<?php endif; ?>
			<button class="btn btn-success btn-sm edit_account" data-utype="<?=$account['usertype']?>" data-uid="<?=$account['uid']?>"><i class="fa fa-edit"></i></button>
			<button class="btn btn-danger btn-sm delete_account" data-utype="<?=$account['usertype']?>" data-uid="<?=$account['uid']?>"><i class="fa fa-trash"></i></button>
		</td>
	</tr>
<?php endforeach;?>