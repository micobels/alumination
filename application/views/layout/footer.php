		<footer>
			Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">UPJump</a>
		</footer>

		<div class="success_notify">
			<div class="row">
				<div class="col-sm-2">
					<i class="fa fa-check"></i>
				</div>
				<div class="col-sm-10">
					<p></p>
				</div>
			</div>
			<i class="fa fa-close remove_notif"></i>
		</div>
	</div>

	<script src="<?=base_url()?>assets/js/jquery-3.2.1.min.js"></script>
	<script src="<?=base_url()?>assets/css/bootstrap4/popper.js"></script>
	<script src="<?=base_url()?>assets/css/bootstrap4/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/TweenMax.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/TimelineMax.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/animation.gsap.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="<?=base_url()?>assets/plugins/Isotope/isotope.pkgd.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/easing/easing.js"></script>
	<script src="<?=base_url()?>assets/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="<?=base_url()?>assets/js/custom.js"></script>
	</body>
	<script>
		var baseUrl = "<?=base_url()?>";
		$('.remove_notif').click(function(){
			$(this).parent().fadeOut();
		})

		initSearch();
		function initSearch()
		{
			if($('.search').length && $('.search_panel').length)
			{
				var search = $('.search');
				var panel = $('.search_panel');

				search.on('click', function()
				{
					panel.toggleClass('active');
				});
			}
		}

		$(document).on('click', '.common_logout', function(){
			console.log('asdsd');
			$.ajax({
				type  	: 'POST',
				url 	: baseUrl+"Login/session_destroy",
				data 	: '',
				cache	: false,
				dataType : 'json',
				success : function(r){
					window.location.href = baseUrl;
				},
				error 	: function(e){
					console.log('error');
				}
			})
		})
	</script>
	<?php if(isset($js_array)):?>
		<?php foreach ($js_array as $key => $path):?>
			<script src="<?=$path?>" ></script>
		<?php endforeach;?>
	<?php endif;?>
</html>