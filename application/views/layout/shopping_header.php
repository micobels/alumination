<!DOCTYPE html>
<html lang="en">
<head>
<title>Alumination PH</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Sublime project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap4/bootstrap.min.css">
<link href="<?=base_url()?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/main_styles.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/responsive.css">
<?php if(isset($css_array)):?>
	<?php foreach ($css_array as $key => $path):?>
		<link rel="stylesheet" type="text/css" href="<?=$path?>">
	<?php endforeach;?>
<?php endif;?>
</head>
<body>

	<style type="text/css">
	.success_notify,
	.error_notify{
        background: #96e194;
        position: fixed;
        right: 20px;
        bottom: 20px;
        padding: 10px;
        box-shadow: 0px 0px 5px 1px #74be74;
        animation: show_notif ease-in-out 1.3s;
        display: none;
        z-index: 10;
    }
    .error_notify{
    	background: #e19494;
    	box-shadow: 0px 0px 5px 1px #be7474;
    }
    .success_notify .row i{
        font-size: 25px;
        color: #347f34;
    }
    .error_notify .row i{
    	font-size: 25px;
        color: #7f3434;
    }
    .success_notify .remove_notif,
    .error_notify .remove_notif{
        position: absolute;
        top: -10px;
        right: -10px;
        padding: 5px 6px;
        background: #f5f5f5;
        color: #000;
        border-radius: 20px;
        cursor: pointer;
        box-shadow: 0 0 1px 1px #96e194;
    }
    .error_notify .remove_notif{
    	box-shadow: 0px 0px 5px 1px #be7474;
    }
    .success_notify p,
    .error_notify p{
        color: #000;
    }
    @keyframes show_notif{
        0%{
            opacity: 0;
            bottom: 0;
        }
        50%{
            opacity: 1;
            bottom: 30px;
        }
        100%{
            opacity: 1;
            bottom: 20px;
        }
    }
	</style>

<div class="super_container">

		<header class="header">
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>assets/img/alumination.png" style="width: 20%;">Alumination<span style="color: #e95a5a"> PH</span></a></div>
							<nav class="main_nav">
								<ul>
									<li><a href="<?=base_url()?>">Home</a></li>
									<li class="hassubs">
										<a href="<?=base_url()?>Categories">Shop</a>
										<ul>
											<li><a href="<?=base_url()?>Categories?type=1">Doors</a></li>
											<li><a href="<?=base_url()?>Categories?type=2">Window</a></li>
											<li><a href="<?=base_url()?>Categories?type=3">Gate</a></li>
											<li><a href="<?=base_url()?>Categories?type=4">Railings</a></li>
										</ul>
									</li>
									<li><a href="<?=base_url()?>About">About Us</a></li>
									<li><a href="<?=base_url()?>Contact">Contact</a></li>
								</ul>
							</nav>
							<div class="header_extra" style="margin-left: 100px;">
								<?php if(isset($user)): ?>
								<div class="search">
									<div class="search_icon">
										<a href="#" style="color: #000;font-weight: 500;">
											<div style="width: 150px;">Welcome <b><?=$user['fname']?></b></div>
										</a>
									</div>
								</div>
								<?php else: ?>
									<a href="<?=base_url()?>Login" style="color: #000;font-weight: 500;display: inline-block;">
										<div>Login</div>
									</a>
									<a href="<?=base_url()?>Registration_payment" style="color: #e95a5a;font-weight: 500;display: inline-block;margin-left: 30px;">Activate</a>
								<?php endif; ?>
								<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Search Panel -->
		<div class="search_panel trans_300">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="search_panel_content d-flex flex-row align-items-center justify-content-end">
							<ul class="list-inline text-right" style="width: 100%;display: contents;">
								<?php if(isset($user) && $user['usertype'] == 3): ?>
									<li style="margin-left: 20px;"><a href="<?=base_url()?>Booked_services">My Booked Services</a></li>
								<?php else: ?>
									<li style="margin-left: 20px;"><a href="<?=base_url()?>Admin/Dashboard">Dashboard</a></li>
								<?php endif; ?>

								<?php if(isset($user) && $user['usertype'] !=1): ?>
									<li style="margin-left: 20px;"><a href="<?=base_url()?>Profile">My Account</a></li>
								<?php endif;?>
								<li style="margin-left: 20px;" class="common_logout"><a href="#">Logout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>