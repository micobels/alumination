		<div class="newsletter">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="newsletter_border"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8 offset-lg-2">
						<div class="newsletter_content text-center">
							<div class="newsletter_title">Want to be a Seller?</div>
							<div class="newsletter_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie eros</p></div>
							<div class="newsletter_form_container">
								<a href="<?=base_url()?>How_to">
									<button class="newsletter_button trans_200"><span>Sell Now</span></button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Footer -->
		
		<div class="footer_overlay"></div>
		<footer class="footer">
			<div class="footer_background" style="background-image:url(<?=base_url()?>assets/img/footer.jpg)"></div>
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="footer_content d-flex flex-lg-row flex-column align-items-center justify-content-lg-start justify-content-center">
							<div class="footer_logo"><a href="#">Alumination PH</a></div>
							<div class="copyright ml-auto mr-auto">
								Copyright &copy;<script>document.write(new Date().getFullYear());</script>
								All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">UPJump</a>
							</div>
							<div class="footer_social ml-lg-auto">
								<ul>
									<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="success_notify">
		<div class="row">
			<div class="col-sm-2">
				<i class="fa fa-check"></i>
			</div>
			<div class="col-sm-10">
				<p></p>
			</div>
		</div>
		<i class="fa fa-close remove_notif"></i>
	</div>

	<div class="error_notify">
		<div class="row">
			<div class="col-sm-2">
				<i class="fa fa-warning"></i>
			</div>
			<div class="col-sm-10">
				<p></p>
			</div>
		</div>
		<i class="fa fa-close remove_notif"></i>
	</div>

	<script src="<?=base_url()?>assets/js/jquery-3.2.1.min.js"></script>
	<script src="<?=base_url()?>assets/css/bootstrap4/popper.js"></script>
	<script src="<?=base_url()?>assets/css/bootstrap4/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/TweenMax.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/TimelineMax.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/animation.gsap.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="<?=base_url()?>assets/plugins/Isotope/isotope.pkgd.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/easing/easing.js"></script>
	<script src="<?=base_url()?>assets/plugins/parallax-js-master/parallax.min.js"></script>
	</body>

	<?php if(isset($js_array)):?>
		<?php foreach ($js_array as $key => $path):?>
			<script src="<?=$path?>" ></script>
		<?php endforeach;?>
	<?php endif;?>

	<script type="text/javascript">
		var baseUrl = "<?=base_url()?>";
		initSearch();
		function initSearch()
		{
			if($('.search').length && $('.search_panel').length)
			{
				var search = $('.search');
				var panel = $('.search_panel');

				search.on('click', function()
				{
					panel.toggleClass('active');
				});
			}
		}

		$('.remove_notif').click(function(){
			$(this).parent().fadeOut();
		})

		$(document).on('click', '.common_logout', function(){
			console.log('asdsd');
			$.ajax({
				type  	: 'POST',
				url 	: baseUrl+"Login/session_destroy",
				data 	: '',
				cache	: false,
				dataType : 'json',
				success : function(r){
					window.location.href = baseUrl;
				},
				error 	: function(e){
					console.log('error');
				}
			})
		})
	</script>
</html>