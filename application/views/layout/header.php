<!DOCTYPE html>
<html lang="en">
<head>
<title>Alumination PH</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Sublime project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap4/bootstrap.min.css">
<link href="<?=base_url()?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/main_styles.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/responsive.css">
<?php if(isset($css_array)):?>
    <?php foreach ($css_array as $key => $path):?>
        <link rel="stylesheet" type="text/css" href="<?=$path?>">
    <?php endforeach;?>
<?php endif;?>
</head>
<style type="text/css">
    .header_content{
        height: 70px;
    }
    .hassubs ul{
        padding-right: 10px;
        padding-left: 10px;
        width: 170px;
        text-align: center;
        box-shadow: 0px 5px 5px 0px;
    }
    .main_nav{
        margin-left: 100px;
    }
    footer{
        background: #fff;
        padding: 10px;
        position: fixed;
        bottom: 0;
        left: 100px;
        border-radius: 5px 5px 0 0;
        box-shadow: 0px 0px 10px 1px #d3d3d3;
        color: #000;
        font-size: 12px;
    }
    .white_container{
        margin: 100px 20px 50px;
        background: #fff;
        box-shadow: 0px 0px 3px #d3d3d3;
    }
    .white_container .title_holder{
        padding: 10px 20px;
        background: #383838;
    }
    .white_container .title_holder h3{
        padding: 0;
        margin: 0;
        color: #fff;
    }
    .white_container .title_holder i,
    .white_container label{
        color: #e95a5a;
    }
    .white_container .body_container{
        padding: 10px 20px;
    }
    .success_notify{
        background: #96e194;
        position: fixed;
        right: 20px;
        bottom: 20px;
        padding: 10px;
        box-shadow: 0px 0px 5px 1px #74be74;
        animation: show_notif ease-in-out 1.3s;
        display: none;
    }
    .success_notify .row i{
        font-size: 25px;
        color: #347f34;
    }
    .success_notify .remove_notif{
        position: absolute;
        top: -10px;
        right: -10px;
        padding: 5px 6px;
        background: #f5f5f5;
        color: #000;
        border-radius: 20px;
        cursor: pointer;
        box-shadow: 0 0 1px 1px #96e194;
    }
    .success_notify p{
        color: #000;
    }
    @keyframes show_notif{
        0%{
            opacity: 0;
            bottom: 0;
        }
        50%{
            opacity: 1;
            bottom: 30px;
        }
        100%{
            opacity: 1;
            bottom: 20px;
        }
    }
</style>
<body style="background: #f5f5f5;">

<div class="super_container">
    <header class="header">
        <div class="header_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="header_content d-flex flex-row align-items-center justify-content-start">
                            <div class="logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>assets/img/alumination.png" style="width: 20%;">Alumination <span style="color: #e95a5a;">PH</span></a></div>
                            <nav class="main_nav">
                                <ul>
                                    <li><a href="<?=base_url()?>Admin/Dashboard">Dashboard</a></li>
                                    <li class="hassubs">
                                        <a href="#">Products</a>
                                        <ul>
                                            <li><a href="<?=base_url()?>Admin/Products">Product Lists</a></li>
                                            <li><a href="<?=base_url()?>Admin/Products/create_product">Create Product</a></li>
                                            <?php if($user['usertype'] == 1): ?>
                                            <li><a href="<?=base_url()?>Admin/Products/product_request">Seller's Product Request</a></li>
                                        <?php endif; ?>
                                        </ul>
                                    </li>
                                    <?php if($user['usertype'] == 1): ?>
                                    <li class="hassubs">
                                        <a href="#">Accounts</a>
                                        <ul>
                                            <li><a href="<?=base_url()?>Admin/Accounts?type=2">Seller Accounts</a></li>
                                            <li><a href="<?=base_url()?>Admin/Accounts?type=3">Customer Accounts</a></li>
                                            <li><a href="<?=base_url()?>Admin/Accounts/create_account">Create New Account</a></li>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                                    <li class="hassubs">
                                        <a href="#">Services</a>
                                        <ul>
                                            <?php if($user['usertype'] == 1): ?>
                                                <li><a href="<?=base_url()?>Admin/Services/booked_services">Booked Services</a></li>
                                                <li><a href="<?=base_url()?>Admin/Services/cancelled_services">Cancelled Services</a></li>
                                                <li><a href="<?=base_url()?>Admin/Services">Booking Section</a></li>
                                            <?php else: ?>
                                                <li><a href="<?=base_url()?>Admin/Services">My Services</a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                            <div class="header_extra ml-auto">
                                <a style="color: #000;font-weight: 500;">
                                    <div class="search">
                                        <div class="search_icon">
                                            <a href="#" style="color: #000;font-weight: 500;">
                                                <div style="width: 150px;">Welcome <b><?=$user['fname']?></b></div>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Search Panel -->
        <div class="search_panel trans_300">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="search_panel_content d-flex flex-row align-items-center justify-content-end">
                            <ul class="list-inline text-right" style="width: 100%;display: contents;">
                                <li style="margin-left: 20px;"><a href="<?=base_url()?>Admin/Dashboard">Dashboard</a></li>
                                <li style="margin-left: 20px;" class="common_logout"><a href="#">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>