<style type="text/css">
.form-control{
	color: #999;
}
.image_upload_preview ul{
	display: flex;
}
.image_upload_preview li{
	width: 10%;
	margin: 5px;
	border: solid 2px #d3d3d3;
}
.image_upload_preview li:first-child{
	margin-left: 0;
}
.image_upload_preview li img{
	width: 100%;
	height: 120px;
	object-fit: cover;
}
.business_info_container{
	display: none;
}
</style>
<div class="white_container">
	<div class="title_holder"> 
		<h3><i class="fa fa-user"></i> Accounts Creation</h3>
	</div>
	<div class="body_container">
		<form class="admin_registration_form">
			<div class="row">
				<div class="col-sm-8">
					<div class="form-group">
						<label for="user_name">Full Name</label>
						<input type="text" class="form-control" id="user_name" placeholder="User Full Name">
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="user_type">User Type</label>
						<select class="form-control" id="user_type">
							<option value="0">Choose Type</option>
							<option value="1">Admin</option>
							<option value="2">Seller</option>
							<option value="3">Customer</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="user_email">Email Address</label>
						<input type="email" class="form-control" id="user_email" placeholder="User Email">
						<small class="form-text text-muted">Your Email will be your Username.</small>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="user_mobile">Mobile Number</label>
						<input type="number" class="form-control" id="user_mobile" placeholder="User Mobile Number">
					</div>
				</div>
			</div>
			<div class="row business_info_container">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="user_business_name">Business Name</label>
						<input type="email" class="form-control" id="user_business_name" placeholder="Business Name">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="user_business_address">Business Address</label>
						<input type="text" class="form-control" id="user_business_address" placeholder="Business Addressr">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="user_address">Address</label>
						<input type="text" class="form-control" id="user_address" placeholder="User Official Address">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="user_pass">Password</label>
						<input type="password" class="form-control" id="user_pass">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="confirm_pass">Confirm Password</label>
						<input type="password" class="form-control" id="confirm_pass">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-right">
					<div class="form-group">
						<button type="button" class="btn btn-primary proceed_registration">Create Account</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>