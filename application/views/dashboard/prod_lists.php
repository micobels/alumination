<style type="text/css">
	table thead th{
		color: #e95a5a;
	}
	table tbody tr{
		color: #000;
	}
	.pagination{
		margin-top: 30px;
		color: #000;
		font-size: 15px;
	}
	.pagination i{
		margin: auto 20px;
		cursor: pointer;
		font-size: 25px;
	}
	.pagination i:hover{
		color: #e95a5a;
	}
</style>
<div class="white_container">
	<div class="title_holder"> 
		<h3><i class="fa fa-list"></i> Product List</h3>
	</div>
	<div class="body_container">
		<div class="data_container">
			<table class="table table-hover text-center">
		  		<thead>
		    		<tr>
		      			<th scope="col">Product ID</th>
		      			<th scope="col">Product Name</th>
		      			<th scope="col">Seller Details</th>
		      			<th scope="col">Product Category</th>
		      			<th scope="col">Status</th>
		      			<th scope="col">Action</th>
		    		</tr>
		  		</thead>
		  		<tbody>
		  			<?php if(!empty($products)): ?>
			  			<?php foreach ($products as $key => $p):?>
				    		<tr>
				      			<td><?=$p['pid']?></td>
				      			<td><?=$p['name']?></td>
				      			<td class="text-left">
				      				<b>ID</b> : <?=$p['creator']?> <br />
				      				<b>Name</b> : <?=$users[$p['creator']]['fname'] . " " .$users[$p['creator']]['mname'] . " " .$users[$p['creator']]['lname']?> <br />
				      				<b>Email</b> : <?=$users[$p['creator']]['email']?> <br />
				      				<b>Mobile</b> : <?=isset($users[$p['creator']]['mobile']) ? $users[$p['creator']]['mobile'] : ''?> <br />
				      			</td>
				      			<td><?=$categories[$p['type']]?></td>
				      			<td><?=$p['status'] == 1 ? 'Active' : 'Deactivated'?></td>
				      			<td>
				      				<button class="btn btn-success btn-sm edit_prod" data-id="<?=$p['pid']?>"><i class="fa fa-edit"></i></button>
				      				<button class="btn btn-danger btn-sm delete_product" data-id="<?=$p['pid']?>"><i class="fa fa-trash"></i></button>
				      			</td>
				    		</tr>
				    	<?php endforeach;?>
			    	<?php else: ?>
			    		<tr>
		    				<td colspan="7">No Data to Preview</td>
		    			</tr>
			    	<?php endif; ?>
		  		</tbody>
			</table>

			<?php if($pages != 0): ?>
			<div class="pagination" data-status="1">
				<?php if($pages > 1): ?>
				<div class="prev_page paginate">
					<i class="fa fa-angle-left"></i>
				</div>
				<?php endif; ?>
				<div class="details"><span class="cur_page">1</span> of <span class="max_page"><?=$pages?></span></div>
				<?php if($pages > 1): ?>
				<div class="next_page paginate">
					<i class="fa fa-angle-right"></i>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div id="edit_product_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Edit Product</h4>
      		</div>
      		<div class="modal-body">

        		<form class="edit_product_form">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
						    	<label for="prod_name">Product Name</label>
						    	<input type="text" class="form-control" id="prod_name" placeholder="Name of your Product">
						  	</div>
					  	</div>
					</div>
					<div class="row">
					  	<div class="col-sm-12">
					  		<div class="form-group">
					  			<label for="prod_type">Product Type</label>
						    	<select class="form-control" id="prod_type">
						      		<option value="0">Choose Type</option>
						      		<option value="1">Doors</option>
						      		<option value="2">Window</option>
						      		<option value="3">Gate</option>
						      		<option value="4">Railings</option>
						    	</select>
						    </div>
					  	</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="prod_description">Product Description</label>
		    					<textarea class="form-control" id="prod_description" rows="5" placeholder="write your description here."></textarea>
								<small id="descriptionhelp" class="form-text text-muted">Describe your Product properly for this will be the Basis of the Customer to help them avail your service and the Admin for activating your Product.</small>
							</div>
						</div>
					</div>
				</form>

      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-success save_edit">Save</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="confirm_delete_prod" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Confirm Delete</h4>
      		</div>
      		<div class="modal-body">
      			<p>Are you sure you want to Proceed in deleting this product?</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-danger proceed_delete">Proceed</button>
      		</div>
    	</div>
  	</div>
</div>