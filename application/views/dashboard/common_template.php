<?php if($table == 'products_list'): ?>
	<?php if(!empty($products)) : ?>
		<?php foreach ($products as $key => $p):?>
			<tr>
	  			<td><?=$p['pid']?></td>
	  			<td><?=$p['name']?></td>
	  			<td class="text-left">
	  				<b>ID</b> : <?=$p['creator']?> <br />
	  				<b>Name</b> : <?=$users[$p['creator']]['fname'] . " " .$users[$p['creator']]['mname'] . " " .$users[$p['creator']]['lname']?> <br />
	  				<b>Email</b> : <?=$users[$p['creator']]['email']?> <br />
	  				<b>Mobile</b> : <?=$users[$p['creator']]['mobile']?> <br />
	  			</td>
	  			<td><?=$categories[$p['type']]?></td>
	  			<td><?=$p['status'] == 1 ? 'Active' : 'Deactivated'?></td>
	  			<td>
	  				<button class="btn btn-success btn-sm edit_prod" data-id="<?=$p['pid']?>"><i class="fa fa-edit"></i></button>
	  				<button class="btn btn-danger btn-sm delete_product" data-id="<?=$p['pid']?>"><i class="fa fa-trash"></i></button>
	  			</td>
			</tr>
		<?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan="7">No Data to Preview</td>
		</tr>
	<?php endif; ?>
<?php endif ; ?>

<?php if($table == 'product_request'): ?>
	<?php if(!empty($products)): ?>
		<?php foreach ($products as $key => $value):?>
			<tr>
	  			<td><?=$value['pid']?></td>
	  			<td><?=$value['name']?></td>
	  			<td><?=$categories[$value['type']]?></td>
	  			<td class="text-left">
	  				<b>ID</b> : <?=$value['creator']?> <br />
	  				<b>Name</b> : <?=$users[$value['creator']]['fname'] . " " .$users[$value['creator']]['mname'] . " " .$users[$value['creator']]['lname']?> <br />
	  				<b>Email</b> : <?=$users[$value['creator']]['email']?> <br />
	  				<b>Mobile</b> : <?=$users[$value['creator']]['mobile']?> <br />
	  			</td>
	  			<td><?=$value['date']?></td>
	  			<td>
	  				<button class="btn btn-success btn-sm activate_prod" data-id="<?=$value['pid']?>"><i class="fa fa-dashboard"></i></button>
	  				<button class="btn btn-danger btn-sm delete_product" data-id="<?=$value['pid']?>"><i class="fa fa-trash"></i></button>
	  			</td>
			</tr>
		<?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan="6">No Data to Preview</td>
		</tr>
	<?php endif; ?>
<?php endif ; ?>

<?php if($table == 'cancelled'): ?>
	<?php if(!empty($requests)): ?>
		<?php foreach ($requests as $key => $req): ?>
    		<tr>
      			<td><?=$req['rid']?></td>
      			<td class="text-left"><?=$products[$req['pid']]['name']?></td>
      			<td class="text-left">
      				Name: <?=$accounts[$req['uid']]['fname']." ".$accounts[$req['uid']]['mname']." ".$accounts[$req['uid']]['lname']?> <br>
      				Email: <?=$accounts[$req['uid']]['email']?> <br>
      				Mobile: <?=$accounts[$req['uid']]['mobile']?> <br>
      			</td>
      			<td><?=$req['create_date']?></td>
      			<td><?=$status[$req['status']]?></td>
    		</tr>
    	<?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan="6">No Data to Preview</td>
		</tr>
	<?php endif; ?>
<?php endif ; ?>

<?php if($table == 'booked_services'): ?>
	<?php if(!empty($requests)): ?>
		<?php foreach ($requests as $key => $req): ?>
    		<tr>
      			<td><?=$req['rid']?></td>
      			<td class="text-left"><?=$products[$req['pid']]['name']?></td>
      			<td class="text-left">
      				Name: <?=$accounts[$req['uid']]['fname']." ".$accounts[$req['uid']]['mname']." ".$accounts[$req['uid']]['lname']?> <br>
      				Email: <?=$accounts[$req['uid']]['email']?> <br>
      				Mobile: <?=$accounts[$req['uid']]['mobile']?> <br>
      			</td>
      			<td class="text-left">
      				Name: <?=$sellers[$products[$req['pid']]['creator']]['fname']." ".$sellers[$products[$req['pid']]['creator']]['mname']." ".$sellers[$products[$req['pid']]['creator']]['lname']?> <br>
      				Email: <?=$sellers[$products[$req['pid']]['creator']]['email']?> <br>
      				Mobile: <?=$sellers[$products[$req['pid']]['creator']]['mobile']?> <br>
      			</td>
      			<td><?=$req['create_date']?></td>
      			<td><?=$status[$req['status']]?></td>
      			<td>
      				<?php if($req['status'] == 1): ?>
      					<button class="btn btn-primary btn-sm view_image" data-id="<?=$req['rid']?>"><i class="fa fa-image"></i></button>
      					<button class="btn btn-success btn-sm approve_trans" data-id="<?=$req['rid']?>"><i class="fa fa-thumbs-up"></i></button>
      				<?php endif; ?>
      			</td>
    		</tr>
    	<?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan="7">No Data to Preview</td>
		</tr>
	<?php endif; ?>
<?php endif ; ?>

<?php if($table == 'booking_section'): ?>
	<?php if(!empty($requests)): ?>
		<?php foreach ($requests as $key => $req): ?>
    		<tr>
      			<td><?=$req['rid']?></td>
      			<td class="text-left"><?=$products[$req['pid']]['name']?></td>
      			<td class="text-left">
      				Name: <?=$accounts[$req['uid']]['fname']." ".$accounts[$req['uid']]['mname']." ".$accounts[$req['uid']]['lname']?> <br>
      				Email: <?=$accounts[$req['uid']]['email']?> <br>
      				Mobile: <?=$accounts[$req['uid']]['mobile']?> <br>
      			</td>
      			<td><?=$req['create_date']?></td>
      			<td><?=$status[$req['status']]?></td>
      			<td>
      				<?php if($req['status'] == 2): ?>
      					<button class="btn btn-success btn-sm finish_service" data-id="<?=$req['rid']?>"><i class="fa fa-thumbs-up"></i></button>
<!--       					<?php if($user['usertype'] == 1): ?>
      					<button class="btn btn-danger btn-sm delete_account" data-id="<?=$req['rid']?>"><i class="fa fa-ban"></i></button>
      					<?php endif; ?> -->
      				<?php elseif($req['status'] == 3): ?>
      					<button class="btn btn-warning btn-sm preview_feedback" data-id="<?=$req['rid']?>"><i class="fa fa-star"></i></button>
      				<?php endif; ?>
      			</td>
    		</tr>
    	<?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan="6">No Data to Preview</td>
		</tr>
	<?php endif; ?>
<?php endif ; ?>

<?php if($table == 'customer_services'): ?>
	<?php if(!empty($requests)): ?>
		<?php foreach ($requests as $key => $req): ?>
    		<tr>
      			<td><?=$req['rid']?></td>
      			<td><?=$products[$req['pid']]['name']?></td>
      			<td><?=$req['create_date']?></td>
      			<td><?=$status[$req['status']]?></td>
      			<td>
      				<?php if($req['status'] == 0) : ?>
      					<button class="btn btn-success btn-sm upload_resibo" data-id="<?=$req['rid']?>"><i class="fa fa-upload"></i></button>
      					<button class="btn btn-danger btn-sm cancel_service" data-id="<?=$req['rid']?>"><i class="fa fa-ban"></i></button>
      				<?php elseif($req['status'] == 1 ||$req['status'] == 2): ?>
      					<button class="btn btn-primary btn-sm customer_support" data-id="<?=$req['rid']?>"><i class="fa fa-comments"></i></button>
      					<button class="btn btn-danger btn-sm cancel_service" data-id="<?=$req['rid']?>"><i class="fa fa-ban"></i></button>
  					<?php elseif($req['status'] == 3): ?>
      					<button class="btn btn-warning btn-sm open_feedback" data-id="<?=$req['rid']?>"><i class="fa fa-star"></i></button>
      				<?php elseif($req['status'] == 4): ?>
      					<button class="btn btn-primary btn-sm uncancel" data-id="<?=$req['rid']?>"><i class="fa fa-refresh"></i></button>
      				<?php endif; ?>
      			</td>
    		</tr>
    	<?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan="5">No Data to Preview</td>
		</tr>
	<?php endif; ?>
<?php endif ; ?>