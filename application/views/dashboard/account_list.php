<style type="text/css">
	table thead th{
		color: #e95a5a;
	}
	table tbody tr{
		color: #000;
	}
	.pagination{
		margin-top: 30px;
		color: #000;
		font-size: 15px;
	}
	.pagination i{
		margin: auto 20px;
		cursor: pointer;
		font-size: 25px;
	}
	.pagination i:hover{
		color: #e95a5a;
	}
	.business_info_contain{
		display: none;
	}
	.title_holder{
		position: relative;
	}
	.search_container{
		position: absolute;
	    right: 20px;
	    top: -15px;
	    background: #fff;
	    padding: 5px;
	    box-shadow: 0px 0px 5px #383838;
	}
	.search_container button{
		padding: 5px 10px;
	    background: #fff;
	    border: solid 1px #e95a5a;
	    color: #e95a5a;
	    outline: none;
	    border-radius: 50px;
	    cursor: pointer;
	    transition: all ease-in-out 0.3s;
	}
	.search_container button:hover{
		background: #e95a5a;
		color: #fff;
	}
</style>
<div class="white_container">
	<div class="title_holder"> 
		<h3><i class="fa fa-users"></i> Account Lists</h3>
		<div class="search_container">
			<input type="text" class="search_input" placeholder="Search Account Here.">
			<button type="button" class="search_accounts" data-utype="<?=$_GET['type']?>">search</button>
		</div>
	</div>
	<div class="body_container">
		<div class="data_container">
			<table class="table table-hover text-center">
		  		<thead>
		    		<tr>
		      			<th scope="col">Account ID</th>
		      			<th scope="col" style="text-align: left;">Account Description</th>
		      			<th scope="col">Type</th>
		      			<th scope="col">Action</th>
		    		</tr>
		  		</thead>
		  		<tbody class="account_list_table">
		  			<?php foreach ($list as $key => $account): ?>
			    		<tr>
			      			<td><?=$account['uid']?></td>
			      			<td style="text-align: left;">
			      				<b>Name:</b> <?=$account['fname'] ." ". $account['mname'] ." ". $account['lname']?> <br>
			      				<b>Email:</b> <?=$account['email']?> <br>
			      				<b>Mobile:</b> <?=isset($account['mobile']) ? $account['mobile'] : ""?>
			      			</td>
			      			<td><?=$reference[$account['usertype']]?></td>
			      			<td>
			      				<?php if($account['usertype'] == 2): ?>
									<?php if($account['status'] == 1): ?>
									<button class="btn btn-warning btn-sm activate_account" data-uid="<?=$account['uid']?>"><i class="fa fa-dashboard"></i></button>
									<?php endif; ?>
								<?php endif; ?>
			      				<button class="btn btn-success btn-sm edit_account" data-utype="<?=$account['usertype']?>" data-uid="<?=$account['uid']?>"><i class="fa fa-edit"></i></button>
			      				<button class="btn btn-danger btn-sm delete_account" data-utype="<?=$account['usertype']?>" data-uid="<?=$account['uid']?>"><i class="fa fa-trash"></i></button>
			      			</td>
			    		</tr>
			    	<?php endforeach;?>
		  		</tbody>
			</table>

			<div id="edit_product_modal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
							<h4 class="modal-title">Edit Account</h4>
						</div>
						<div class="modal-body" style="height: 430px;overflow: auto;">

							<form class="edit_account_form">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">First Name</label>
											<input type="text" class="form-control" id="edit_fname">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">Middle Name</label>
											<input type="text" class="form-control" id="edit_mname">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">Last Name</label>
											<input type="text" class="form-control" id="edit_lname">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">Address No.</label>
											<input type="text" class="form-control" id="edit_address_no">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">Barangay</label>
											<input type="text" class="form-control" id="edit_address_barangay">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">City/Municipality</label>
											<input type="text" class="form-control" id="edit_address_city">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">State</label>
											<input type="text" class="form-control" id="edit_address_state">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">Mobile</label>
											<input type="text" class="form-control" id="edit_mobile">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="prod_name">E-mail</label>
											<input type="text" class="form-control" id="edit_email">
										</div>
									</div>
								</div>
								<div class="business_info_contain">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="edit_business_name">Business Name</label>
												<input type="text" class="form-control" id="edit_business_name">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="edit_business_address">Business Address</label>
												<input type="text" class="form-control" id="edit_business_address">
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-success save_edit_account">Save</button>
						</div>
					</div>
				</div>
			</div>

			<div class="pagination account_list_pagination">
				<div class="prev_page page">
					<i class="fa fa-angle-left"></i>
				</div>
				<div class="details">
					<span class="current_page">1</span> of <span class="total_page"><?=$total_pages?></span>
				</div>
				<div class="next_page page">
					<i class="fa fa-angle-right"></i>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="view_activate_details" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
				<h4 class="modal-title">Validate Account Payment</h4>
			</div>
			<div class="modal-body" style="height: 430px;overflow: auto;">
				<p>Below is the Receipt Uploaded by the Seller. if you wish to approve this transaction, click the button below to activate its account.</p>
				<img src="" style="width: 100%;padding: 30px;">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-success validate_acc_payment">Validate</button>
			</div>
		</div>
	</div>
</div>