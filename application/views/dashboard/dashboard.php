<style type="text/css">
	.mid_container{
		background: url(../assets/img/dashboard.png) center center no-repeat;
		min-height: 50vh;
		position: fixed;
		bottom: 20%;
		left: 50%;
		transform: translate(-50%,0);
		background-size: contain;
		text-align: center;
		color: #000;
		animation: enter ease-in-out 2s;
	}
	.mid_container h3{
		margin-top: 100px;
		animation: enter_left ease-in-out 2s;
	}
	.mid_container h1{
		font-weight: 900;
		animation: enter_right ease-in-out 2s;
	}
	@keyframes enter{
		0%{
			opacity: 0;
			bottom: 10%;
		}
		100%{
			opacity: 1;
			bottom: 20%;
		}
	}
	@keyframes enter_left{
		0%{
			opacity: 0;
			margin-left: -50px;
		}
		100%{
			opacity: 1;
			margin-left: 0;
		}
	}
	@keyframes enter_right{
		0%{
			opacity: 0;
			margin-right: -50px;
		}
		100%{
			opacity: 1;
			margin-right: 0;
		}
	}
</style>
<div class="mid_container">
	<h3>WELCOME</h3>
	<h1>Alumination <span style="color: #e95a5a;font-weight: 900;">PH</span></h1>
</div>