<style type="text/css">
	table thead th{
		color: #e95a5a;
	}
	table tbody tr{
		color: #000;
	}
	.pagination{
		margin-top: 30px;
		color: #000;
		font-size: 15px;
	}
	.pagination i{
		margin: auto 20px;
		cursor: pointer;
		font-size: 25px;
	}
	.pagination i:hover{
		color: #e95a5a;
	}
</style>
<div class="white_container">
	<div class="title_holder"> 
		<h3><i class="fa fa-legal"></i> Seller's Product Requests</h3>
	</div>
	<div class="body_container">
		<div class="data_container">
			<table class="table table-hover text-center">
		  		<thead>
		    		<tr>
		      			<th scope="col">Product ID</th>
		      			<th scope="col">Product Name</th>
		      			<th scope="col">Product Category</th>
		      			<th scope="col">Seller</th>
		      			<th scope="col">Date of Request</th>
		      			<th scope="col">Action</th>
		    		</tr>
		  		</thead>
		  		<tbody>
		  			<?php if(!empty($products)):?>
		  				<?php foreach ($products as $key => $value) : ?>
				    		<tr>
				      			<td><?=$value['pid']?></td>
				      			<td><?=$value['name']?></td>
				      			<td><?=$categories[$value['type']]?></td>
				      			<td class="text-left">
					  				<b>ID</b> : <?=$value['creator']?> <br />
					  				<b>Name</b> : <?=$users[$value['creator']]['fname'] . " " .$users[$value['creator']]['mname'] . " " .$users[$value['creator']]['lname']?> <br />
					  				<b>Email</b> : <?=$users[$value['creator']]['email']?> <br />
					  				<b>Mobile</b> : <?=$users[$value['creator']]['mobile']?> <br />
					  			</td>
				      			<td><?=$value['date']?></td>
				      			<td>
				      				<button class="btn btn-success btn-sm activate_prod" data-id="<?=$value['pid']?>"><i class="fa fa-dashboard"></i></button>
				      				<button class="btn btn-danger btn-sm delete_product" data-id="<?=$value['pid']?>"><i class="fa fa-trash"></i></button>
				      			</td>
				    		</tr>
			    		<?php endforeach; ?>
		    		<?php else :?>
		    			<tr>
		    				<td colspan="6">No Data to Preview</td>
		    			</tr>
		    		<?php endif; ?>
		  		</tbody>
			</table>

			<?php if($pages != 0): ?>
			<div class="pagination">
				<?php if($pages > 1): ?>
				<div class="prev_page paginate">
					<i class="fa fa-angle-left"></i>
				</div>
				<?php endif; ?>
				<div class="details"><span class="cur_page">1</span> of <span class="max_page"><?=$pages?></span></div>
				<?php if($pages > 1): ?>
				<div class="next_page paginate">
					<i class="fa fa-angle-right"></i>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div id="confirm_delete_prod" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Confirm Delete</h4>
      		</div>
      		<div class="modal-body">
      			<p>Are you sure you want to Proceed in deleting this product?</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-danger proceed_delete">Proceed</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="confirm_activation" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
        		<h4 class="modal-title">Confirm Product Activation</h4>
      		</div>
      		<div class="modal-body">
      			<p>Once the Product would be Activated, it will appear in the Shop. Are your sure you want to continue?</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-success proceed_activation">Proceed</button>
      		</div>
    	</div>
  	</div>
</div>