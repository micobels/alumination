<style type="text/css">
	.form-control{
		color: #999;
	}
	.image_upload_preview ul{
		display: flex;
	}
	.image_upload_preview li{
		width: 10%;
		margin: 5px;
		border: solid 2px #d3d3d3;
	}
	.image_upload_preview li:first-child{
		margin-left: 0;
	}
	.image_upload_preview li img{
		width: 100%;
		height: 120px;
    	object-fit: cover;
	}
</style>
<div class="white_container">
	<div class="title_holder"> 
		<h3><i class="fa fa-cubes"></i> Product Creation</h3>
	</div>
	<div class="body_container">
		<form method="POST" action="#" enctype="multipart/form-data" class="create_product_form" id="create_product_form">
			<div class="row">
				<div class="col-sm-8">
					<div class="form-group">
				    	<label for="prod_name">Product Name</label>
				    	<input type="text" class="form-control" id="prod_name" placeholder="Name of your Product">
				  	</div>
			  	</div>
			  	<div class="col-sm-4">
			  		<div class="form-group">
			  			<label for="prod_type">Product Type</label>
				    	<select class="form-control" id="prod_type">
				      		<option value="0">Choose Type</option>
				      		<option value="1">Doors</option>
				      		<option value="2">Window</option>
				      		<option value="3">Gate</option>
				      		<option value="4">Railings</option>
				    	</select>
				    </div>
			  	</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="prod_description">Product Description</label>
    					<textarea class="form-control" id="prod_description" rows="5" placeholder="write your description here."></textarea>
						<small id="descriptionhelp" class="form-text text-muted">Describe your Product properly for this will be the Basis of the Customer to help them avail your service and the Admin for activating your Product.</small>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label>Product Images</label><br>
						<div class="image_upload_preview">
							<ul class="list-inline img_preview_container"></ul>
						</div>
						<label class="btn btn-info" for="upload_prod_images" style="color: #fff;">Upload Images</label>
						<input type="file" id="upload_prod_images" name="prod_images[]" accept="image/*" multiple style="display: none;">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-right">
					<div class="form-group">
						<button type="submit" class="btn btn-primary save_product">Create Product</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>