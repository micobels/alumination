<style type="text/css">
	table thead th{
		color: #e95a5a;
	}
	table tbody tr{
		color: #000;
	}
	.pagination{
		margin-top: 30px;
		color: #000;
		font-size: 15px;
	}
	.pagination i{
		margin: auto 20px;
		cursor: pointer;
		font-size: 25px;
	}
	.pagination i:hover{
		color: #e95a5a;
	}
</style>
<div class="white_container">
	<div class="title_holder"> 
		<h3><i class="fa fa-users"></i> Account Lists</h3>
	</div>
	<div class="body_container">
		<div class="data_container">
			<table class="table table-hover text-center">
		  		<thead>
		    		<tr>
		      			<th scope="col">Request ID</th>
		      			<th scope="col" class="text-left">Service Name</th>
		      			<th scope="col" class="text-left">Client Details</th>
		      			<th scope="col">Date</th>
		      			<th scope="col">Status</th>
		      			<!-- <th scope="col">Action</th> -->
		    		</tr>
		  		</thead>
		  		<tbody class="account_list_table">
		  			<?php if(!empty($requests)):?>
			  			<?php foreach ($requests as $key => $req): ?>
				    		<tr>
				      			<td><?=$req['rid']?></td>
				      			<td class="text-left"><?=$products[$req['pid']]['name']?></td>
				      			<td class="text-left">
				      				Name: <?=$accounts[$req['uid']]['fname']." ".$accounts[$req['uid']]['mname']." ".$accounts[$req['uid']]['lname']?> <br>
				      				Email: <?=$accounts[$req['uid']]['email']?> <br>
				      				Mobile: <?=$accounts[$req['uid']]['mobile']?> <br>
				      			</td>
				      			<td><?=$req['create_date']?></td>
				      			<td><?=$status[$req['status']]?></td>
				      			<!-- <td>
				      				<?php if($req['status'] == 1): ?>
				      					<button class="btn btn-primary btn-sm view_image" data-id="<?=$req['rid']?>"><i class="fa fa-image"></i></button>
				      					<button class="btn btn-success btn-sm approve_trans" data-id="<?=$req['rid']?>"><i class="fa fa-thumbs-up"></i></button>
				      				<?php endif; ?>
				      				<button class="btn btn-danger btn-sm delete_account" data-id="<?=$req['rid']?>"><i class="fa fa-ban"></i></button>
				      			</td> -->
				    		</tr>
				    	<?php endforeach;?>
				    <?php else: ?>
			    		<tr>
							<td colspan="6">No Data to Preview</td>
						</tr>
			    	<?php endif; ?>
		  		</tbody>
			</table>

			<div class="pagination account_list_pagination" data-type="cancelled">
				<div class="paginate prev_page">
					<i class="fa fa-angle-left"></i>
				</div>
				<div class="details">
					<span class="cur_page">1</span> of <span class="max_page"><?=$pages?></span>
				</div>
				<div class="paginate next_page">
					<i class="fa fa-angle-right"></i>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="preview_image" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
	  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
	    		<h4 class="modal-title">Receipt Uploaded</h4>
	  		</div>
	  		<div class="modal-body text-center">
	  			<img src="#" style="width: 100%;height: 50vh;object-fit: contain;">
	  		</div>
	  		<div class="modal-footer">
	    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  		</div>
    	</div>
  	</div>
</div>

<div id="approve_prod_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
	  		<div class="modal-header" style="border-bottom: solid #e95a5a;color: #000;">
	    		<h4 class="modal-title">Approve Transaction</h4>
	  		</div>
	  		<div class="modal-body text-center">
	  			Once Transaction is Approved, It will be moved to the 'Booking Section'. The Seller Will be Notified of this. <br>Are you sure you want to Proceed?
	  		</div>
	  		<div class="modal-footer">
	    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    		<button type="button" class="btn btn-success proceed_approve">Proceed</button>
	  		</div>
    	</div>
  	</div>
</div>