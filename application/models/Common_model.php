<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model{

	public function alph_insert($table, $data){
		$insert = $this->db->insert($table, $data);
		if ($insert) {
		    $insert_id = $this->db->insert_id();
			return $insert_id;
		}else{
			return false;
		}
	} # end alph_insert

	public function alph_delete($table, $data){
		$delete = $this->db->delete($table, $data);
		if ($delete) {
			return true;
		}else{
			return false;
		}
	} # end alph_delete

	public function alph_update($table, $data, $param){
		return $this->db->update($table, $data, $param);
	} # end alph_update

	public function get_all_data($table, $where = [],$order_by = [], $paging = [], $fields = [], $search = []){
		if(!empty($fields)){
			$this->db->select($fields);
		}else{
			$this->db->select('*');
		}
		$this->db->from($table);
		
		if (!empty($where)) {
            foreach ($where as $key => $val) {
                if (is_array($val)) {
                	if(!empty($val)){
                    	$this->db->where_in($key, $val);
                	}
                } else {
                	if ($key!="") {
                		$this->db->where($key, $val);
                	}else{
                		$this->db->where($val);
                	}
                    
                }
            }
        }

		if (!empty($order_by)) {
            if (is_array($order_by)) {
                foreach ($order_by as $o => $b) {
                    if (is_numeric($o)) {
                        $this->db->order_by($b, 'asc');
                    } else {
                        $this->db->order_by($o, $b);
                    }
                }
            } else {
                $this->db->order_by($order_by);
            }
        }

        if(!empty($search)){
        	$like_statements = array();

            foreach ($search as $key => $value) {
                $like_statements[] = "{$key} LIKE '%{$value}%'";
            }
            $like_string = "(" . implode(' OR ', $like_statements) . ")";
            $this->db->where($like_string);
        }

        if (!empty($paging['per_page'])) {
            $this->db->limit($paging['per_page'], $paging['segment']);
        }
		
		$query = $this->db->get();
		return $query->result_array();
    } # end get_all_data
} # end class