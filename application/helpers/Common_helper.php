<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	function json_response($status, $message = '', $data = [], $err_code = ''){
		if ($data == []) {
			$data = new stdClass();
		}
		$return = [];
		$return = [
			'status'   => $status,
			'message'  => $message,
			'list'     => $data,
			'err_code' => $err_code,
		];
		echo json_encode($return);
		die();
	}

	function set_key(array $array, $key,$int_vals = array()){
        if (!$array || !is_array($array)) {
            die("Array required on set_key function.");
        }
        foreach ($array as $arr) {
            if (!is_array($arr)) {
                die("All contents must be array in order to use set_key function.");
            }
            if (!isset($arr[$key])) {
                // die("Key not found in one or more array contents.");
                die();
            }
        }
        $new_array = [];
        foreach ($array as $arr_val) {
            if($int_vals && is_array($int_vals) ){
                foreach ($int_vals as $iv_val) {
                    if(isset ($arr_val[$iv_val]) )
                           $arr_val[$iv_val] = intval($arr_val[$iv_val]);
                }
            }
            $new_array[$arr_val[$key]] = $arr_val;
        }
        return $new_array;
    }

    function get_pages($total, $limit)
    {
        $quotient =(double)$total/(double)$limit;
        $int_quotient = (int) $quotient;
        $init_page_count = (double)$int_quotient;
        if ($quotient > $init_page_count) return (int)$init_page_count+1;
        else if($quotient == $init_page_count) return (int)$init_page_count;
    }
?>