<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller{

    public function index(){
        $user           = $this->session->userdata('logged_in');
        $data           = [];
        $data['user']   = $user;
        
        $data['css_array'] = [base_url().'assets/css/contact.css'];

        $this->load->view('layout/shopping_header',$data);
        $this->load->view('shopping/about_shop');
        $this->load->view('layout/shopping_footer');
    }
}