<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->scripts = [
        	base_url().'/assets/js/jquery-3.2.1.min.js',
        	base_url().'/assets/js/auth/authentication.js',
        ];
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');
    }

    public function index(){
        if(!$this->session->userdata('logged_in')){
            $data             = [];
            $data['js_array'] = $this->scripts;
            $this->load->view('login', $data);
        }else{
            if($this->session->userdata('logged_in')['usertype'] == 1){
                redirect('/Admin/Dashboard', 'location', 302);
            }else{
                redirect('/', 'location', 302);
            }
        }
    }

    public function authenticate(){
        $info = $this->input->post('info');

        $retrieved_info = $this->common->get_all_data('alph_accounts', ['email' => $info['email']] );
        
        if(empty($retrieved_info)){
            $retrieved_info = $this->common->get_all_data('alph_accounts_customer', ['email' => $info['email']] );
            if(empty($retrieved_info)){
                $retrieved_info = $this->common->get_all_data('alph_accounts_seller', ['email' => $info['email']] );
            }
        }
        if($retrieved_info){
            $to_check_password = hash('sha256', $info['password'] . $retrieved_info[0]['salt']);
            for($round = 0; $round < 65536; $round++){
                $to_check_password = hash('sha256', $to_check_password);
            }
            if($to_check_password == $retrieved_info[0]['password']){
                if($retrieved_info[0]['usertype'] == 2){
                    if($retrieved_info[0]['status'] == 2){
                        $this->session->set_userdata('logged_in',$retrieved_info[0]);
                        json_response('success', 'User Found', []);
                    }else{
                        json_response('failed','Account is Currently Disabled.');
                    }
                }else{
                    $this->session->set_userdata('logged_in',$retrieved_info[0]);
                    json_response('success', 'User Found', []);
                }
            }else{
                json_response('failed', 'Wrong Password', []);
            }
        }else{
            json_response('failed', 'No existing account', []);
        }
    }

    public function session_destroy(){
        session_destroy();
        json_response('success', 'Logged out', []);
    }
}
