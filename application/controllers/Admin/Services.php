<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller{
    public function __construct(){
        parent::__construct();
            
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');

        $this->user = $this->session->userdata('logged_in');
        $this->limit = 10;
    }

    public function index(){
        if($this->session->userdata('logged_in') && in_array($this->session->userdata('logged_in')['usertype'], [1,2])){
            $user           = $this->session->userdata('logged_in');
            $data           = [];
            $where = [];
            $data['user']   = $user;
            $data['js_array'] = [base_url().'assets/js/booked_services.js'];

            $where['status'] = [2,3];

            if($user['usertype'] == 2){
                $user_prods = $this->common->get_all_data('alph_products',['creator' => $user['uid']],[],[],['pid']);
                $prod_array = array_column($user_prods, 'pid');
                $where['pid'] = $prod_array;
            }
            
            $data['requests'] = $this->common->get_all_data('alph_requests',$where,['rid' => 'DESC'],['per_page' => $this->limit, 'segment' => 0]);
            $total_list = $this->common->get_all_data('alph_requests',$where,[],[],['count(rid) as total'])[0]['total'];
            $data['pages'] = get_pages($total_list, $this->limit);

            $products = $this->common->get_all_data('alph_products',['status' => 1]);
            $data['products'] = $products ? set_key($products,'pid') : [];

            $accounts = $this->common->get_all_data('alph_accounts_customer',['usertype' => 3]);
            $data['accounts'] = $accounts ? set_key($accounts,'uid') : [];

            $data['status'] = array(
                0 => 'Upload Payment / Pending',
                1 => 'Paid / For Admin Approval',
                2 => 'Admin Approved / Pending Service',
                3 => 'Service Successfully Provided',
                4 => 'Cancelled'
            );

            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/booking_section');
            $this->load->view('layout/footer');
        }else{
            redirect('/');
        }
    }

    public function retrieve_image(){
        $id = $this->input->post('id');
        $image = $this->common->get_all_data('alph_requests',['rid' => $id],[],[],['image'])[0];
        json_response('success','',$image);
    }

    public function approve_prod(){
        $id = $this->input->post('id');
        $this->common->alph_update('alph_requests',['status' => 2],['rid' => $id]);
        json_response('success');
    }

    public function booked_services(){
        if($this->session->userdata('logged_in') && $this->session->userdata('logged_in')['usertype'] == 1){
            $user           = $this->session->userdata('logged_in');
            $data           = [];
            $data['user']   = $user;
            
            $data['js_array'] = [base_url().'assets/js/booked_services.js'];
            $data['requests'] = $this->common->get_all_data('alph_requests',['status' => [0,1]],['rid' => 'DESC'],['per_page' => $this->limit, 'segment' => 0]);
            $total_list       = $this->common->get_all_data('alph_requests',['status' => [0,1]],[],[],['count(rid) as total'])[0]['total'];
            $data['pages']    = get_pages($total_list, $this->limit);

            $products         = $this->common->get_all_data('alph_products',['status' => 1]);
            $data['products'] = $products ? set_key($products,'pid') : [];
            
            $accounts         = $this->common->get_all_data('alph_accounts_customer',['usertype' => 3]);
            $data['accounts'] = $accounts ? set_key($accounts,'uid') : [];
            $accounts         = $this->common->get_all_data('alph_accounts_seller');
            $data['sellers'] = $accounts ? set_key($accounts,'uid') : [];

            $data['status'] = array(
                0 => 'Upload Payment / Pending',
                1 => 'Paid / For Admin Approval',
                2 => 'Approved / Pending Service',
                3 => 'Service Successfully Provided',
                4 => 'Cancelled'
            );

            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/booked_services');
            $this->load->view('layout/footer');
        }else{
            redirect('/');
        }
    }

    public function finish_trans(){
        $id = $this->input->post('id');
        $this->common->alph_update('alph_requests',['status' => 3],['rid' => $id]);
        json_response('success');
    }

    public function retrieve_feedback(){
        $id = $this->input->post('id');
        $feedback = $this->common->get_all_data('alph_requests',['rid' => $id],[],[],['feedback'])[0];
        json_response('success','',$feedback);
    }

    public function cancelled_services(){
        if($this->session->userdata('logged_in') && $this->session->userdata('logged_in')['usertype'] == 1){
            $user           = $this->session->userdata('logged_in');
            $data           = [];
            $data['user']   = $user;
            
            $data['js_array'] = [base_url().'assets/js/booked_services.js'];
            $data['requests'] = $this->common->get_all_data('alph_requests',['status' => 4],['rid' => 'DESC'],['per_page' => $this->limit, 'segment' => 0]);
            $total_list = $this->common->get_all_data('alph_requests',['status' => 4],[],[],['count(rid) as total'])[0]['total'];
            $data['pages'] = get_pages($total_list, $this->limit);

            $products = $this->common->get_all_data('alph_products',['status' => 1]);
            $data['products'] = $products ? set_key($products,'pid') : [];

            $accounts = $this->common->get_all_data('alph_accounts_customer',['usertype' => 3]);
            $data['accounts'] = $accounts ? set_key($accounts,'uid') : [];

            $data['status'] = array(
                0 => 'Upload Payment / Pending',
                1 => 'Paid / For Admin Approval',
                2 => 'Approved / Pending Service',
                3 => 'Service Successfully Provided',
                4 => 'Cancelled'
            );

            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/cancelled_services');
            $this->load->view('layout/footer');
        }else{
            redirect('/');
        }
    }

    public function load_table(){
        $page = $this->input->post('page');
        $type = $this->input->post('type');
        $offset = ($page-1)*$this->limit;
        $where = [];
        $data  = [];
        
        $user           = $this->session->userdata('logged_in');
        $data['user']   = $user;

        if($type == 'cancelled'){
            $where['status'] = 4;
            $data['table'] = 'cancelled';
        }
        else if($type == 'booked_services'){
            $where['status'] = [0,1];
            $data['table'] = 'booked_services';
        }
        else if($type == 'booking_section'){
            $where['status'] = [2,3];
            $data['table'] = 'booking_section';
            if($user['usertype'] == 2){
                $user_prods = $this->common->get_all_data('alph_products',['creator' => $user['uid']],[],[],['pid']);
                $prod_array = array_column($user_prods, 'pid');
                $where['pid'] = $prod_array;
            }
        }else if($type == 'customer_services'){
            $data['table'] = 'customer_services';
            $where['uid'] = $user['uid'];
        }
        $data['requests'] = $this->common->get_all_data('alph_requests',$where,['rid' => 'DESC'],['per_page' => $this->limit, 'segment' => $offset]);

        $total_list = $this->common->get_all_data('alph_requests',$where,[],[],['count(rid) as total'])[0]['total'];
        $data['pages'] = get_pages($total_list, $this->limit);

        // References
        $products = $this->common->get_all_data('alph_products',['status' => 1]);
        $data['products'] = set_key($products,'pid');

        $accounts = $this->common->get_all_data('alph_accounts_customer',['usertype' => 3]);
        $data['accounts'] = set_key($accounts,'uid');

        $sellers         = $this->common->get_all_data('alph_accounts_seller');
        $data['sellers'] = $sellers ? set_key($sellers,'uid') : [];

        $data['status'] = array(
            0 => 'Upload Payment / Pending',
            1 => 'Paid / For Admin Approval',
            2 => 'Approved / Pending Service',
            3 => 'Service Successfully Provided',
            4 => 'Cancelled'
        );

        $this->load->view('dashboard/common_template',$data);
    }

}