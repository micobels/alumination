<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

    public function index(){
    	if($this->session->userdata('logged_in') && in_array($this->session->userdata('logged_in')['usertype'], [1,2])){
	        $data           = [];
	        $data['user'] = $this->session->userdata['logged_in'];
	        $this->load->view('layout/header',$data);
	        $this->load->view('dashboard/dashboard');
	        $this->load->view('layout/footer');
	    }else{
	    	redirect('/');
	    }
    }
}