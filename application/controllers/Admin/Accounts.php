<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Common_model', 'common');
        $this->scripts = array(
            base_url()."assets/js/admin/account_management.js",
        );
        $this->limit = 10;
        $this->table = 'accounts_seller';
        if(isset($_GET['type']) && $_GET['type'] == 3){
            $this->table = 'accounts_customer';
        }
    }

    public function index(){
        if($this->session->userdata('logged_in') && $this->session->userdata('logged_in')['usertype'] == 1){
            $data                    = [];
            $footer_data             = [];
            $footer_data['js_array'] = $this->scripts;

            $list                    = $this->common->get_all_data($this->table, [], [], ['per_page' => $this->limit, 'segment' => 0]);
            $total_page              = $this->common->get_all_data($this->table, [], [], [], ['count(*) as total']);
            $reference = array(
                1 => "Admin",
                2 => "Seller",
                3 => "Customer",
            );
            $data['list']        = $list;
            $data['total_pages'] = get_pages($total_page[0]['total'], $this->limit);
            $data['reference']   = $reference;
            $data['user']  = $this->session->userdata('logged_in');

            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/account_list', $data);
            $this->load->view('layout/footer', $footer_data);
        }else{
            redirect('/');
        }
    } # end of index

    public function load_table(){
        $data    = [];
        $search    = [];
        $to_page = $this->input->post('page');
        $params  = $this->input->post('params');
        $offset  = ($to_page-1)*$this->limit;

        if($params){
            $search['uid'] = $params;
            $search['fname'] = $params;
            $search['mname'] = $params;
            $search['lname'] = $params;
            $search['email'] = $params;
            $search['mobile'] = $params;
            $search['address_no'] = $params;
            $search['address_barangay'] = $params;
            $search['address_city'] = $params;
            $search['address_state'] = $params;
        }

        $list = $this->common->get_all_data($this->table, [], [], ['per_page' => $this->limit, 'segment' => $offset], [], $search);

        $reference = array(
            1 => "Admin",
            2 => "Seller",
            3 => "Customer",
        );
        $data['list']      = $list;
        $data['reference']   = $reference;
        $this->load->view('templates/account_list_template', $data);
    }

    public function create_account(){
        if($this->session->userdata('logged_in') && $this->session->userdata('logged_in')['usertype'] == 1){
            $data                    = [];
            $footer_data             = [];
            $footer_data['js_array'] = $this->scripts;
            $data['user']  = $this->session->userdata('logged_in');

            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/create_account', $data);
            $this->load->view('layout/footer',$footer_data);
        }else{
            redirect('/');
        }
    } # end of create_account

    public function save_account(){
        $params = $_POST['info'];
        $salt     = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
        $password = hash('sha256', $params['password'] . $salt);
        for($round = 0; $round < 65536; $round++){
            $password = hash('sha256', $password);
        }

        $account_data = array(
            'fullname' => $params['fullname'],
            'email' => $params['email'],
            'mobile' => $params['mobile'],
            'address' => $params['address'],
            'password' => $password,
            'salt' => $salt,
            'usertype' => $params['usertype'],
        );

        if($params['usertype'] == 2){
            $account_data['business_address'] = $params['business_address'];
            $account_data['business_name'] = $params['business_name'];
        }

        $saved = $this->common->alph_insert('alph_accounts', $account_data);
        if($saved){
            json_response('true', 'success');
        }
        json_response('false', 'error', [], 'ec#001');
    } # end save_account

    public function get_user_data(){
        $user_id = $this->input->post('id');
        $info = $this->common->get_all_data($this->table, ['uid' => $user_id]);

        json_response('true', 'success', $info, 'ec#000');
    } # end get_user_data

    public function update_user_data(){
        $user_data = $this->input->post('content');


        $account_data = array(
            'fname'            => $user_data['fname'],
            'mname'            => $user_data['mname'],
            'lname'            => $user_data['lname'],
            'email'            => $user_data['email'],
            'mobile'           => $user_data['mobile'],
            'address_no'       => $user_data['address_no'],
            'address_barangay' => $user_data['address_barangay'],
            'address_city'     => $user_data['address_city'],
            'address_state'    => $user_data['address_state'],
        );

        if($user_data['utype'] == 2){
            $account_data['business_address'] = $user_data['business_address'];
            $account_data['business_name'] = $user_data['business_name'];
            $this->common->alph_update('alph_accounts_seller', $account_data, ['uid' => $user_data['id']]);
        }else{
            $this->common->alph_update('alph_accounts_customer', $account_data, ['uid' => $user_data['id']]);
        }

        json_response('true', 'success', [], 'ec#000');
    } # end update_user_data

    public function delete_user_data(){
        $user_id = $this->input->post('id');
        $user_type = $this->input->post('utype');

        if($user_type == 2){
            $this->common->alph_delete('alph_accounts_seller', ['uid' => $user_id]);
        }else{
            $this->common->alph_delete('alph_accounts_customer', ['uid' => $user_id]);
        }
        $this->common->alph_delete('alph_products', ['creator' => $user_id]);
        json_response('true', 'success', [], 'ec#000');
    } # end delete_user_data
}