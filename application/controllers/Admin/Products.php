<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->scripts = [
            base_url().'assets/js/admin/products_management.js'
        ];
            
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');

        $this->user = $this->session->userdata('logged_in');
        $this->limit = 10;
    }

    public function index(){
        if($this->session->userdata('logged_in') && in_array($this->session->userdata('logged_in')['usertype'], [1,2])){
            $data           = [];
            $where = [];
            $data['js_array'] = $this->scripts;

            if($this->user['usertype'] != 1){
                $where['creator'] = $this->user['uid'];
            }else{
                $where['status'] = 1;
            }
            $data['user'] = $this->user;

            $data['products'] = $this->common->get_all_data('alph_products',$where,['pid' => 'DESC'],['per_page' => $this->limit, 'segment' => 0]);
            $total_list = $this->common->get_all_data('alph_products',$where,[],[],['count(pid) as total'])[0]['total'];
            $data['pages'] = get_pages($total_list, $this->limit);

            $data['categories'] = array(
                '1' => 'Doors',
                '2' => 'Window',
                '3' => 'Gate',
                '4' => 'Railings'
            );

            $user = $this->common->get_all_data('alph_accounts_seller');
            $data['users'] = set_key($user,'uid');
            
            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/prod_lists', $data);
            $this->load->view('layout/footer');
        }else{
            redirect('/');
        }
    }

    public function create_product(){
        if($this->session->userdata('logged_in') && in_array($this->session->userdata('logged_in')['usertype'], [1,2])){
            $data           = [];
            $data['js_array'] = $this->scripts;
            $data['user'] = $this->user;
            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/create_prod', $data);
            $this->load->view('layout/footer',$data);
        }else{
            redirect('/');
        }
    }

    public function product_request(){
        if($this->session->userdata('logged_in') && $this->session->userdata('logged_in')['usertype'] == 1){
        	$data           = [];
            $data['js_array'] = $this->scripts;
            $data['user'] = $this->user;

            $data['products'] = $this->common->get_all_data('alph_products',['status' => 0],['pid' => 'DESC'],['per_page' => $this->limit, 'segment' => 0]);
            $total_list = $this->common->get_all_data('alph_products',['status' => 0],[],[],['count(pid) as total'])[0]['total'];
            $data['pages'] = get_pages($total_list, $this->limit);

            $data['categories'] = array(
                '1' => 'Doors',
                '2' => 'Window',
                '3' => 'Gate',
                '4' => 'Railings'
            );

            $user = $this->common->get_all_data('alph_accounts_seller');
            $data['users'] = set_key($user,'uid');

            $this->load->view('layout/header',$data);
            $this->load->view('dashboard/prod_request',$data);
            $this->load->view('layout/footer');
        }else{
            redirect('/');
        }
    }

    public function save_product(){
        extract($_POST);
        if($_FILES){
            if($_FILES['prod_images']){
                $prod_images       = $_FILES['prod_images'];
                $prod_images_total = count($_FILES['prod_images']['name']);
                $prod_images_saved = [];
                for( $i=0 ; $i < $prod_images_total ; $i++ ) {
                    $temp_file_path = $prod_images['tmp_name'][$i];

                    if ($temp_file_path != "" && $name != ''){
                        $final_file_name  = $name . $i;
                        $extension        = explode("/", $prod_images["type"][$i]);
                        $upload_dir_image = FCPATH . 'assets/img/products/'.str_replace(" ", "-", $name)."/";

                        if (!file_exists($upload_dir_image)) {
                            mkdir($upload_dir_image, 0755, true);
                        }

                        $newFilePath = "assets/img/products/".str_replace(" ", "-", $name)."/". str_replace(" ", "-", $final_file_name).".".$extension[1];
                        array_push($prod_images_saved, $newFilePath);

                        if(move_uploaded_file($temp_file_path, $newFilePath)) {
                            chmod(FCPATH .$newFilePath, 0755);
                        }
                    }
                }
            }
        }

        $product_info = array(
            'name' => $name,
            'type' => $type,
            'description' => $desc,
            'images' => json_encode($prod_images_saved),
            'creator' => $this->user['uid'],
        );

        $saved = $this->common->alph_insert('alph_products', $product_info);
        if($saved){
            json_response('true', 'success');
        }
        json_response('false', 'error', [], 'ec#001');
    }

    public function get_data_prod(){
        $id = $this->input->post('id');

        $data = $this->common->get_all_data('alph_products',['pid' => $id]);
        json_response('','',$data);
    }

    public function save_edit_prod(){
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $type = $this->input->post('type');
        $desc = $this->input->post('desc');

        $data = array(
            'name' => $name,
            'type' => $type,
            'description' => $desc
        );

        $this->common->alph_update('alph_products',$data,['pid' => $id]);
        json_response('success');
    }

    public function delete_prod(){
        $id = $this->input->post('id');
        $this->common->alph_delete('alph_products',['pid'=>$id]);
        json_response('success');
    }

    public function load_table(){
        $page = $this->input->post('page');
        $status = $this->input->post('status');
        $offset = ($page-1)*$this->limit;
        $data['table'] = $status == 1 ? 'products_list' : 'product_request';
        $where = [];

        $where['status'] = $status;

        $data['categories'] = array(
            '1' => 'Doors',
            '2' => 'Window',
            '3' => 'Gate',
            '4' => 'Railings'
        );

        $user = $this->common->get_all_data('alph_accounts_seller');
        $data['users'] = set_key($user,'uid');

        $data['products'] = $this->common->get_all_data('alph_products',$where,['pid' => 'DESC'],['per_page' => $this->limit, 'segment' => $offset]);

        $this->load->view('dashboard/common_template',$data);
    }

    public function activate_prod(){
        $id = $this->input->post('id');
        $this->common->alph_update('alph_products',['status' => 1],['pid' => $id]);
        json_response('success');
    }
}