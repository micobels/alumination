<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller{

	public function __construct(){
        parent::__construct();
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');
    }

    public function info($id){
        $user           = $this->session->userdata('logged_in');
        $data           = [];
        $data['user']   = $user;
        
        $data['js_array'] = [base_url().'assets/js/product.js'];
        $data['css_array'] = [base_url().'assets/css/product.css'];

        $data['item'] = $this->common->get_all_data('alph_products',['pid' => $id])[0];
        $request = $this->common->get_all_data('alph_requests',['pid' => $data['item']['pid'], 'uid' => $user['uid']]);

        if(!empty($request)){
            $data['request'] = $request[0];
            $data['seller'] = $this->common->get_all_data('alph_accounts_seller',['uid' => $data['item']['creator']])[0];
        }

        $data['products'] = $this->common->get_all_data('alph_products',['status' => 1,'type' => $data['item']['type'],'pid !=' => $id],['pid' => 'RAND'],['per_page' => 4,'segment' => 0]);

        $this->load->view('layout/shopping_header',$data);
        $this->load->view('shopping/single_product',$data);
        $this->load->view('layout/shopping_footer');
    }

    public function booking_service(){
        $id = $this->input->post('id');
        $user = $this->session->userdata('logged_in');

        $data = array(
            'uid' => $user['uid'],
            'pid' => $id,
            'status' => 0,
        );
        
        $this->common->alph_insert('alph_requests',$data);
        json_response('success');
    }
}