<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller{
    public function __construct(){
        parent::__construct();
            
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');

        $this->user = $this->session->userdata('logged_in');
    }

    public function index(){
        if($this->session->userdata('logged_in')){
            $data           = [];
            $data['user'] = $this->user;

            $table = 'alph_accounts_seller';
            if($this->user['usertype'] == 3){
                $table = 'alph_accounts_customer';
            }
            $data['user_data'] = $this->common->get_all_data($table,['uid' => $this->user['uid']])[0];

            $data['js_array'] = [base_url().'assets/js/profile.js'];

            $this->load->view('layout/shopping_header',$data);
            $this->load->view('shopping/profile', $data);
            $this->load->view('layout/shopping_footer',$data);
        }else{
            redirect('/');
        }
    }

    public function edit_profile(){
        extract($_POST);

        $update_data = array(
            'fname' => $fname,
            'mname' => $mname,
            'lname' => $lname,
            'email' => $email,
            'mobile' => isset($mobile) ? $mobile : '',
            'business_name' => isset($bus_name) ? $bus_name : '',
            'business_address' => isset($bus_add) ? $bus_add : '',
        );

        if(isset($password)){
            $salt     = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
            $new_password = hash('sha256', $password . $salt);
            for($round = 0; $round < 65536; $round++){
                $new_password = hash('sha256', $new_password);
            }

            $update_data['password'] = $new_password;
            $update_data['salt'] = $salt;
        }
        $table = 'alph_accounts_seller';
        if($this->user['usertype'] == 3){
            $table = 'alph_accounts_customer';
        }
        $this->common->alph_update($table,$update_data,['uid' => $this->user['uid']]);
        session_destroy();
        json_response('success');
    }

}