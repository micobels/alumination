<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_payment extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');
    }

    public function index(){
        $user           = $this->session->userdata('logged_in');
        $data           = [];
        $data['user']   = $user;
        
        $data['css_array'] = [base_url().'assets/css/contact.css'];
        $data['js_array'] = [base_url().'assets/js/contact.js'];
        
        $this->load->view('layout/shopping_header',$data);
        $this->load->view('shopping/registration_payment');
        $this->load->view('layout/shopping_footer');
    }

    public function change_status(){
        extract($_POST);

        $retrieved_info = $this->common->get_all_data('alph_accounts', ['email' => $email]);
        
        if(empty($retrieved_info)){
            $retrieved_info = $this->common->get_all_data('alph_accounts_customer', ['email' => $email] );
            if(empty($retrieved_info)){
                $retrieved_info = $this->common->get_all_data('alph_accounts_seller', ['email' => $email] );
            }
        }
        if($retrieved_info && $retrieved_info[0]['usertype'] == 2){
            $to_check_password = hash('sha256', $password . $retrieved_info[0]['salt']);
            for($round = 0; $round < 65536; $round++){
                $to_check_password = hash('sha256', $to_check_password);
            }
            if($to_check_password == $retrieved_info[0]['password']){
                
                if($_FILES){
                    if($_FILES['acc_receipt']){
                        $acc_receipt       = $_FILES['acc_receipt'];
                        $acc_receipt_total = count($_FILES['acc_receipt']['name']);
                        $acc_receipt_saved = '';
                        for( $i=0 ; $i < $acc_receipt_total ; $i++ ) {
                            $temp_file_path = $acc_receipt['tmp_name'];

                            if ($temp_file_path != ""){
                                $final_file_name  = $retrieved_info[0]['uid'] . $i;
                                $extension        = explode("/", $acc_receipt["type"]);
                                $upload_dir_image = FCPATH . 'assets/img/accounts/'.str_replace(" ", "-", $retrieved_info[0]['uid'])."/";

                                if (!file_exists($upload_dir_image)) {
                                    mkdir($upload_dir_image, 0755, true);
                                }
                                $newFilePath = "assets/img/accounts/".str_replace(" ", "-", $retrieved_info[0]['uid'])."/". str_replace(" ", "-", $final_file_name).".".$extension[1];
                                $acc_receipt_saved = $newFilePath;

                                if(move_uploaded_file($temp_file_path, $newFilePath)) {
                                    chmod(FCPATH .$newFilePath, 0755);
                                }

                                $this->common->alph_update('alph_accounts_seller',['receipt_image' => $acc_receipt_saved,'status' => 1],['uid' => $retrieved_info[0]['uid']]);
                                json_response('success','Account Successfully Activated',[]);
                            }
                        }
                    }
                }

            }else{
                json_response('failed', 'Wrong Password', []);
            }
        }else{
            json_response('failed', 'No existing account', []);
        }
    }

    public function fetch_image(){
        $uid = $this->input->post('id');
        $img = $this->common->get_all_data('alph_accounts_seller',['uid' => $uid],[],[],['receipt_image']);
        json_response('success','',$img);
    }

    public function validate_acc_payment(){
        $uid = $this->input->post('id');
        $this->common->alph_update('alph_accounts_seller',['status' => 2],['uid' =>$uid]);
        json_response('success','Account Successfully Validated.');
    }
}