<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booked_services extends CI_Controller{

	public function __construct(){
        parent::__construct();
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');
        $this->limit = 10;
    }

    public function index(){
        if($this->session->userdata('logged_in')){
            $user           = $this->session->userdata('logged_in');
            $data           = [];
            $data['user']   = $user;
            
            $data['js_array'] = [base_url().'assets/js/booked_services.js'];
            $data['requests'] = $this->common->get_all_data('alph_requests',['uid' => $user['uid']],['rid' => 'DESC'],['per_page' => $this->limit, 'segment' => 0]);
            $total_list = $this->common->get_all_data('alph_requests',['uid' => $user['uid']],[],[],['count(pid) as total'])[0]['total'];
            $data['pages'] = get_pages($total_list, $this->limit);

            $products = $this->common->get_all_data('alph_products',['status' => 1]);
            $data['products'] = set_key($products,'pid');

            $data['status'] = array(
            	0 => 'Upload Payment / Pending',
            	1 => 'Paid / For Admin Approval',
            	2 => 'Approved / Pending Service',
            	3 => 'Service Successfully Provided',
            	4 => 'Cancelled'
            );

            $this->load->view('layout/shopping_header',$data);
            $this->load->view('shopping/booked_services');
            $this->load->view('layout/shopping_footer');
        }else{
            redirect('/');
        }
    }

    public function upload_receipt(){
    	extract($_POST);

    	if($_FILES){
            if($_FILES['reciept_image']){
                $upload_dir_image = FCPATH . 'assets/img/receipts/';

                if (!file_exists($upload_dir_image)) {
                    mkdir($upload_dir_image, 0755, true);
                }

                $newFilePath = "assets/img/receipts/".$id.'_'.$_FILES['reciept_image']['name'];

                if(move_uploaded_file($_FILES['reciept_image']['tmp_name'], $newFilePath)) {
                    chmod(FCPATH .$newFilePath, 0755);
                }

                // $new_data= array('image' => $newFilePath,'status' => 1);
                // $this->common->alph_update('alph_requests',$new_data,['rid' => $id]);
                $insert_data = array(
                    'uid' => $this->session->userdata('logged_in')['uid'],
                    'pid' => $id,
                    'status' => 1,
                    'image' => $newFilePath
                );
                $this->common->alph_insert('alph_requests',$insert_data);
                json_response('success');
            }
        }
    }

    public function cancel_service(){
        $id = $this->input->post('id');
        $this->common->alph_update('alph_requests',['status' => 4],['rid' => $id]);
        json_response('success');
    }

    public function uncancel_service(){
        $id = $this->input->post('id');
        $this->common->alph_update('alph_requests',['status' => 1],['rid' => $id]);
        json_response('success');
    }

    public function submit_feedback(){
        $id = $this->input->post('id');
        $feedback = $this->input->post('feedback');
        $this->common->alph_update('alph_requests',['feedback' => $feedback],['rid' => $id]);
        json_response('success');
    }

    public function retrieve_feedback(){
        $id = $this->input->post('id');
        $feedback = $this->common->get_all_data('alph_requests',['rid' => $id],[],[],['feedback']);
        json_response('success','',$feedback);
    }

}