<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');
    }
    public function index(){
        $user           = $this->session->userdata('logged_in');
        $data           = [];
        $data['user']   = $user;
        $data['js_array'] = [base_url().'assets/js/custom.js'];
        
        $data['products'] = $this->common->get_all_data('alph_products',['status' => 1],['pid' => 'RAND'],['per_page' => 8,'segment' => 0]);

        $this->load->view('layout/shopping_header',$data);
        $this->load->view('shopping/main_shop',$data);
        $this->load->view('layout/shopping_footer');
    }
}