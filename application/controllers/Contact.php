<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller{

    public function index(){
        $user           = $this->session->userdata('logged_in');
        $data           = [];
        $data['user']   = $user;
        
        $data['js_array'] = [base_url().'assets/js/contact.js'];
        $data['css_array'] = [base_url().'assets/css/contact.css'];
        $this->load->view('layout/shopping_header',$data);
        $this->load->view('shopping/contact_us');
        $this->load->view('layout/shopping_footer');
    }
}