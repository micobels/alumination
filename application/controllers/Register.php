<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->scripts = array(
            base_url()."assets/js/jquery-3.2.1.min.js",
            base_url()."assets/js/auth/authentication.js",
        );
        $this->load->model('Common_model', 'common');
    }

    public function index(){
        $data           = [];
        $data['js_array'] = $this->scripts;
        $this->load->view('register', $data);
    }

    public function register_account(){
        $params = $_POST['info'];
        $salt     = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
        $password = hash('sha256', $params['password'] . $salt);
        for($round = 0; $round < 65536; $round++){
            $password = hash('sha256', $password);
        }

        $account_data = array(
            'fname' => $params['fname'],
            'mname' => $params['mname'],
            'lname' => $params['lname'],
            'email' => $params['email'],
            'mobile' => $params['mobile'],
            'address_no' => $params['address_no'],
            'address_barangay' => $params['address_barangay'],
            'address_city' => $params['address_city'],
            'address_state' => $params['address_state'],
            'password' => $password,
            'salt' => $salt,
            'usertype' => $params['usertype'],
        );

        if($params['usertype'] == 2){
            $account_data['business_address'] = $params['business_address'];
            $account_data['business_name'] = $params['business_name'];

            $saved = $this->common->alph_insert('alph_accounts_seller', $account_data);
        }else{
            $saved = $this->common->alph_insert('alph_accounts_customer', $account_data);
        }
        
        if($saved){
            json_response('true', 'success', [], 'ec#001');
        }
    } # end save_account
}
