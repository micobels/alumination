<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class How_to extends CI_Controller{

    public function index(){
        $user           = $this->session->userdata('logged_in');
        $data           = [];
        $data['user']   = $user;
        
        $data['css_array'] = [base_url().'assets/css/contact.css'];
        $data['js_array'] = [base_url().'assets/js/contact.js'];
        
        $this->load->view('layout/shopping_header',$data);
        $this->load->view('shopping/be_seller');
        $this->load->view('layout/shopping_footer');
    }
}