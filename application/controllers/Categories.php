<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller{

	public function __construct(){
        parent::__construct();
        $this->load->model('Common_model', 'common');
        $this->load->helper('Common_helper');
        $this->limit = 12;
        $this->scripts = array(
            base_url()."assets/js/shop_management.js",
            base_url()."assets/js/categories.js"
        );
    }

    public function index(){
        $user           = $this->session->userdata('logged_in');
        $data           = [];
        $where          = [];
        $data['user']   = $user;
        $footer_data['js_array'] = $this->scripts;

        $data['css_array'] = [base_url().'assets/css/categories.css'];

        $where['status'] = 1;
        if(isset($_GET['type']) && $_GET['type'] != 0){
            $where['type'] = $_GET['type'];
        }
        $data['products']       = $this->common->get_all_data('alph_products',$where,['pid' => 'RAND'],['per_page' => $this->limit,'segment' => 0]);

        $data['products_total'] = $this->common->get_all_data('alph_products',$where,['pid' => 'RAND'],[],['count(*) as total']);
        $data['products_pages'] = get_pages($data['products_total'][0]['total'], $this->limit);
        $this->load->view('layout/shopping_header',$data);
        $this->load->view('shopping/shop',$data);
        $this->load->view('layout/shopping_footer', $footer_data);
    }

    public function shop_paginate(){
        $data           = [];
        $search           = [];
        $where          = [];
        $page = $this->input->post('page');
        $params = $this->input->post('params');

        if($params){
            $search['name'] = $params;
            $search['description'] = $params;
        }

        $offset = ($page-1)*$this->limit;
        $where['status'] = 1;
        if(isset($_GET['type']) && $_GET['type'] != 0){
            $where['type'] = $_GET['type'];
        }

        $data['products']       = $this->common->get_all_data('alph_products',$where,['pid' => 'RAND'],['per_page' => $this->limit,'segment' => $offset], [], $search);
        $data['products_total'] = $this->common->get_all_data('alph_products',$where,['pid' => 'RAND'],[],['count(*) as total'],$search);
        $data['products_pages'] = get_pages($data['products_total'][0]['total'], $this->limit);
        $data['active_page']    = $page;
        $data['type_search']    = isset($_GET['type']) ? $_GET['type'] : '';

        $this->load->view('templates/shop_list_template',$data);
    }
}